<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Boutique;
use App\Models\CategorieBoutique;
use App\Models\CategorieProduit;
use App\Models\CategorieBlog;
use App\Models\GallerieBoutique;
use App\Models\Produit;
use App\Models\Article;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boutique_3 = Boutique::join('categorie_boutiques', 'boutiques.categorie_boutique_id', 'categorie_boutiques.id')
            ->join('pack_abonnements', 'boutiques.pack_boutique_id', 'pack_abonnements.id')
            ->where([
                ['boutiques.is_active_boutique', true],
                ['pack_abonnements.nom_pack_abonnement', 'Premium']
                ])
            ->select('boutiques.*', 'categorie_boutiques.nom_categorie_boutique')
            ->orderBy('created_at', 'DESC')
            ->take(6)->get();

        $boutique_r = Boutique::join('categorie_boutiques', 'boutiques.categorie_boutique_id', 'categorie_boutiques.id')
            ->where('boutiques.is_active_boutique', true)
            ->select('boutiques.*', 'categorie_boutiques.nom_categorie_boutique')
            ->orderBy('created_at', 'DESC')
            ->take(6)->get();

        $articles = Article::join('users', 'articles.auteur', 'users.id')
                            ->select('articles.*', 'users.prenom_user', 'users.nom_user')
                            ->orderBy('created_at', 'DESC')
                            ->take(3)->get();
        $categories = CategorieBoutique::withCount('boutiques')->take(6)->get();
        return view('index', compact('boutique_3', 'categories', 'articles', 'boutique_r'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_blog()
    {
        $last_article = Article::join('categorie_blogs', 'articles.categorie_id', 'categorie_blogs.id')
                        ->join('users', 'articles.auteur', 'users.id')
                        ->select('articles.*', 'categorie_blogs.slug_categorie_blog', 'categorie_blogs.nom_categorie_blog', 'users.prenom_user', 'users.nom_user')
                        ->latest()->first();
                        // dd($last_article);
        $categories = CategorieBlog::all();
        $blogs = Article::join('categorie_blogs', 'articles.categorie_id', 'categorie_blogs.id')
                        ->join('users', 'articles.auteur', 'users.id')
                        ->select('articles.*', 'categorie_blogs.slug_categorie_blog', 'categorie_blogs.nom_categorie_blog', 'users.prenom_user', 'users.nom_user')
                        ->paginate(6);
        $newArticles = Article::join('categorie_blogs', 'articles.categorie_id', 'categorie_blogs.id')
                            ->select('articles.*', 'categorie_blogs.slug_categorie_blog')
                            ->orderBy('created_at', 'DESC')->take(3)->get();

        // var_dump($last_article);
        return view('blog', compact('categories', 'blogs', 'last_article', 'newArticles'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blog_categorie_index($slug_categorie_blog)
    {
        $categorie = CategorieBlog::where('slug_categorie_blog', $slug_categorie_blog)->first();
        $categories = CategorieBlog::all();
        $blogs = Article::join('categorie_blogs', 'articles.categorie_id', 'categorie_blogs.id')
                        ->join('users', 'articles.auteur', 'users.id')
                        ->select('articles.*', 'categorie_blogs.slug_categorie_blog', 'categorie_blogs.nom_categorie_blog', 'users.prenom_user', 'users.nom_user')
                        ->where('categorie_blogs.slug_categorie_blog', $slug_categorie_blog)
                        ->paginate(6);
        $newArticles = Article::join('categorie_blogs', 'articles.categorie_id', 'categorie_blogs.id')
                            ->select('articles.*', 'categorie_blogs.slug_categorie_blog')
                            ->orderBy('created_at', 'DESC')->take(3)->get();
        return view('blog-categorie', compact('categories', 'blogs', 'newArticles', 'categorie'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blog_index($slug_categorie_blog, $slug_article)
    {
        $categorie = CategorieBlog::where('slug_categorie_blog', $slug_categorie_blog)->first();
        $article = Article::join('users', 'articles.auteur', 'users.id')
                            ->join('categorie_blogs', 'articles.categorie_id', 'categorie_blogs.id')
                            ->select('articles.*', 'categorie_blogs.nom_categorie_blog', 'categorie_blogs.slug_categorie_blog', 'users.prenom_user', 'users.nom_user')
                            ->where('slug_article', $slug_article)->first();
        $posts = Article::join('users', 'articles.auteur', 'users.id')
        ->join('categorie_blogs', 'articles.categorie_id', 'categorie_blogs.id')
        ->select('articles.*', 'categorie_blogs.nom_categorie_blog', 'categorie_blogs.slug_categorie_blog', 'users.prenom_user', 'users.nom_user')
        ->inRandomOrder()->limit(3)->get();
        return view('blog-single', compact('categorie', 'article', 'posts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categorie()
    {
        $categories = CategorieBoutique::withCount('boutiques')->get();
        return view('categorie', compact('categories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categorie_index($slug_categorie_boutique)
    {
        $boutique_4 = Boutique::where('is_active_boutique', true)
            ->orderBy('created_at', 'DESC')->take(3)->get();
        $categorie = CategorieBoutique::where('slug_categorie_boutique', $slug_categorie_boutique)->first();
        $boutiques = Boutique::join('categorie_boutiques', 'boutiques.categorie_boutique_id', 'categorie_boutiques.id')
                            // ->join('produits','boutique_id','boutiques.id')
                            ->where('categorie_boutiques.slug_categorie_boutique', $slug_categorie_boutique)
                            ->select('boutiques.*','categorie_boutiques.*')->paginate(6);

                            // ,'produits.*'
        // dd($boutiques);
        return view('categorie-item', compact('categorie', 'boutiques' ,'boutique_4'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function boutique_index($slug_nom_boutique)
    {
        $b = Boutique::where('slug_nom_boutique', $slug_nom_boutique)->first();
        $boutique = Boutique::join('categorie_boutiques', 'boutiques.categorie_boutique_id', 'categorie_boutiques.id')
                            ->where('slug_nom_boutique', $slug_nom_boutique)
                            ->select('boutiques.*', 'categorie_boutiques.nom_categorie_boutique', 'categorie_boutiques.slug_categorie_boutique')->first();
        $galleries = GallerieBoutique::where('boutique_id', $b->id)->get();
        $produits = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
                            ->where('boutiques.slug_nom_boutique', $slug_nom_boutique)
                            ->select('produits.*')->get();

        $shop_rs = Boutique::where('is_active_boutique', true)->inRandomOrder()->limit(3)->get();
        $like_shops = Boutique::join('pack_abonnements', 'boutiques.pack_boutique_id', 'pack_abonnements.id')
                            ->join('categorie_boutiques', 'boutiques.categorie_boutique_id', 'categorie_boutiques.id')
                            ->where([
                                ['categorie_boutiques.nom_categorie_boutique', $boutique->nom_categorie_boutique],
                                ['boutiques.slug_nom_boutique', '!==', $slug_nom_boutique],
                                ['is_active_boutique', true]
                            ])->select('boutiques.*', 'categorie_boutiques.nom_categorie_boutique')->get();

        return view('infoboutique', compact('boutique', 'galleries', 'produits', 'shop_rs', 'like_shops'));
    }


    public function search(Request $request)
    {
        $search_query =$request->search_query;
        $produits = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
            ->where([
                ['produits.nom_produit', 'like', '%'.$search_query.'%'],
                ['boutiques.is_active_boutique', true]
                ])
            ->select('produits.*', 'boutiques.slug_nom_boutique')->paginate(9);
        return view('recherche', ['produits' => $produits], compact('search_query'));
    }

    public function produit_index($slug_nom_boutique, $slug_nom_produit)
    {

        $boutique = Boutique::where('slug_nom_boutique', $slug_nom_boutique)->first();
        $produit = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
            ->where(
                [
                    ['produits.slug_nom_produit', $slug_nom_produit],
                    ['boutiques.slug_nom_boutique', $slug_nom_boutique],
                ]
                )->first();

        $categorie = CategorieProduit::where('id', $produit->categorie_produit_id)->first();
        $produit_smlaires = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
            ->where('categorie_produit_id', $categorie->id)
            ->select('produits.*', 'boutiques.slug_nom_boutique')->get();
        return view('info-produit', compact('boutique', 'produit', 'produit_smlaires'));
    }

    public function shop_of_z_day()
    {
        $boutique = Boutique::join('pack_abonnements', 'boutiques.pack_boutique_id', 'pack_abonnements.id')
                ->where(
                    [
                        ['boutiques.is_active_boutique', true],
                        ['pack_abonnements.nom_pack_abonnement', '!=', 'Gratuit']
                    ]
                    )
                ->select('boutiques.*')
                ->inRandomOrder()->first();

        $galleries = GallerieBoutique::where('boutique_id', $boutique->id)->get();
        $produits = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
                            ->where('boutiques.slug_nom_boutique', $boutique->slug_nom_boutique)
                            ->select('produits.*')->get();
        $user = Boutique::join('users', 'boutiques.user_boutique_id', 'users.id')
                    ->where('boutiques.id', $boutique->id)
                    ->select('users.*')->first();

        // dd($boutique);
        return view('shop-of-z-day', compact('boutique', 'galleries', 'produits', 'user'));
    }
}
