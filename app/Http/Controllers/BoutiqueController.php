<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Boutique;
use App\Models\PackAbonnement;
use App\Models\CategorieBoutique;
use App\Models\GallerieBoutique;
use App\User;
use Illuminate\Support\Facades\File;

use Session;
use Illuminate\Support\Str;

class BoutiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boutiques = Boutique::paginate(10);

        return view('Admin.boutique.index', compact('boutiques'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategorieBoutique::all();
        $packs = PackAbonnement::all();
        $users = User::join('roles', 'users.role_id', 'roles.id')->where('code_role', 'CL')
                ->select('users.*')->get();
        return view('Admin.boutique.create', compact('categories', 'packs', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_boutique_id' => 'required',
            'categorie_boutique_id' => 'required',
            'nom_boutique' => 'required|unique:boutiques,nom_boutique',
            'adresse_boutique' => 'required',
            'ville_boutique' => 'required',
            'telephone_boutique' => 'required',
            'email_boutique' => 'required',
            'description_boutique' => 'required',
            'pack_boutique_id' => 'required',
        ]);

        $boutique = Boutique::create([
            'user_boutique_id' => $request->user_boutique_id,
            'categorie_boutique_id' => $request->categorie_boutique_id,
            'nom_boutique' => $request->nom_boutique,
            'slug_nom_boutique' => Str::slug($request->nom_boutique, '-'),
            'adresse_boutique' => $request->adresse_boutique,
            'ville_boutique' => $request->ville_boutique,
            'telephone_boutique' => $request->telephone_boutique,
            'email_boutique' => $request->email_boutique,
            'site_web_boutique' => $request->site_web_boutique,
            'link_facebook_boutique' => $request->link_facebook_boutique,
            'link_instagram_boutique' => $request->link_instagram_boutique,
            'link_twitter_boutique' => $request->link_twitter_boutique,
            'jour_ouvrable_boutique' => $request->jour_ouvrable_boutique,
            'open_time_boutique' => $request->open_time_boutique,
            'close_time_boutique' => $request->close_time_boutique,
            'description_boutique' => $request->description_boutique,
            'map_url_boutique' => $request->map_url_boutique,
            'pack_boutique_id' => $request->pack_boutique_id,
            'is_active_boutique' => false,
        ]);

        if ($request->hasFile('photo_boutique')) {
            $photo = $request->photo_boutique;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/logo/', $image_new_name);
            $boutique->photo_boutique = '/public/storage/boutique/logo/'.$image_new_name;
        }

        if ($request->hasFile('galleries')) {
            $images = $request->galleries;
            foreach ($images as $img) {
                $image_new_name = Str::random(10) . '.' . $img->getClientOriginalExtension();
                $img->move('public/storage/boutique/gallerie/', $image_new_name);
                GallerieBoutique::create(
                    [
                        'boutique_id' =>  $boutique->id,
                        'photo_gallerie' => '/public/storage/boutique/gallerie/'.$image_new_name,
                    ]
                );
            }
        }

        $boutique->save();
        Session::flash('success', 'Une nouvelle boutique vient d\'être crée avec succès');

        return redirect()->route('boutique.index');
    }


    /**
     * Enable or Disable user account.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statut(Request $request)
    {
        $boutique = Boutique::where('id', $request->id)->first();

        if($boutique != null){
            if($request->is_active_boutique == "0"){
                $boutique->is_active_boutique = true;
                $boutique->save();
                Session::flash('success', 'La boutique a été activée avec succès');
            }
            elseif($request->is_active_boutique == "1"){
                $boutique->is_active_boutique = false;
                $boutique->save();
                Session::flash('success', 'La boutique a été désactivée avec succès');
            }

            return redirect()->route('boutique.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boutique = Boutique::where('id', $id)->first();
        $categories = CategorieBoutique::all();
        $packs = PackAbonnement::all();
        $gallerieImages = GallerieBoutique::where('boutique_id', $id)->get();
        $users = User::join('roles', 'users.role_id', 'roles.id')->where('code_role', 'CL')
            ->select('users.*')->get();
        return view('Admin.boutique.edit', compact('gallerieImages', 'categories', 'packs', 'users', 'boutique'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $boutique = Boutique::where('id', $id)->first();
        $this->validate($request, [
            'user_boutique_id' => 'required',
            'categorie_boutique_id' => 'required',
            'nom_boutique' => "required|unique:boutiques,nom_boutique,$boutique->id",
            'adresse_boutique' => 'required',
            'ville_boutique' => 'required',
            'telephone_boutique' => 'required',
            'email_boutique' => 'required',
            'description_boutique' => 'required',
            'pack_boutique_id' => 'required',
        ]);

        $boutique->user_boutique_id = $request->user_boutique_id;
        $boutique->categorie_boutique_id = $request->categorie_boutique_id;
        $boutique->nom_boutique = $request->nom_boutique;
        $boutique->slug_nom_boutique = Str::slug($request->nom_boutique, '-');
        $boutique->adresse_boutique = $request->adresse_boutique;
        $boutique->ville_boutique = $request->ville_boutique;
        $boutique->telephone_boutique = $request->telephone_boutique;
        $boutique->email_boutique = $request->email_boutique;
        $boutique->site_web_boutique = $request->site_web_boutique;
        $boutique->link_facebook_boutique = $request->link_facebook_boutique;
        $boutique->link_instagram_boutique = $request->link_instagram_boutique;
        $boutique->link_twitter_boutique = $request->link_twitter_boutique;
        $boutique->jour_ouvrable_boutique = $request->jour_ouvrable_boutique;
        $boutique->open_time_boutique = $request->open_time_boutique;
        $boutique->close_time_boutique = $request->close_time_boutique;
        $boutique->description_boutique = $request->description_boutique;
        $boutique->map_url_boutique = $request->map_url_boutique;
        $boutique->pack_boutique_id = $request->pack_boutique_id;
        $boutique->is_active_boutique = false;

        if($request->hasFile('photo_boutique')){
            $photo = $request->photo_boutique;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/logo/', $image_new_name);
            $boutique->photo_boutique = '/public/storage/boutique/logo/'.$image_new_name;
        }

        if ($request->hasFile('galleries')) {
            $images = $request->galleries;
            foreach ($images as $img) {
                $image_new_name = Str::random(10) . '.' . $img->getClientOriginalExtension();
                $img->move('public/storage/boutique/gallerie/', $image_new_name);
                GallerieBoutique::create(
                    [
                        'boutique_id' =>  $boutique->id,
                        'photo_gallerie' => '/public/storage/boutique/gallerie/'.$image_new_name,
                    ]
                );
            }
        }

        $boutique->save();
        Session::flash('success', 'La boutique vient d\'être modifiée avec succès');

        return redirect()->route('boutique.index');
    }


    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeImageFromGallerie(Request $request)
    {
        // dd($request->id);
        $image = GallerieBoutique::where('id', $request->id)->delete();

        if ($image) {
            return response()->json( array('success'=>true) );
        }

        return response()->json( array('success'=>false) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boutique = Boutique::find($id);

        $currentLogo = $boutique->photo_boutique;
        // $currentGallerie = $boutique->photo_boutique;
        $userLogo = 'https://www.boutiquesenegal.com/'.$currentLogo;
        if (File::exists($userLogo)) {
            File::delete($userLogo);
        }
        $boutique->delete();

        Session::flash('success', 'Une boutique a été supprimée avec succès');
        return redirect()->route('boutique.index');
    }
}
