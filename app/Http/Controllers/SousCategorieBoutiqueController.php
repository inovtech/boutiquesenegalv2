<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\SousCategorieBoutique;
use App\Models\CategorieBoutique;
use Session;


class SousCategorieBoutiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat_boutiques = SousCategorieBoutique::paginate(10);

        return view('Admin.CategorieBoutique.SousCategorie.index', compact('cat_boutiques'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = CategorieBoutique::all();
        return view('Admin.CategorieBoutique.SousCategorie.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom_sous_categorie_boutique' => 'required|unique:sous_categorie_boutiques,nom_sous_categorie_boutique',
            'categorie_boutique_id' => 'required',
        ]);

        SousCategorieBoutique::create([
            'nom_sous_categorie_boutique' => $request->nom_sous_categorie_boutique,
            'slug_sous_categorie_boutique' => Str::slug($request->nom_sous_categorie_boutique, '-'),
            'categorie_boutique_id' => $request->categorie_boutique_id,
        ]);
        Session::flash('success', 'Une nouvelle sous categorie de boutique vient d\'être crée avec succès');

        return redirect()->route('sous-categorie-boutique.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat_boutiques = SousCategorieBoutique::where('id', $id)->first();
        // dd($cat_boutiques);
        $categories = CategorieBoutique::all();
        return view('Admin.CategorieBoutique.SousCategorie.edit', compact('cat_boutiques', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat_boutiques = SousCategorieBoutique::where('id', $id)->first();

        $this->validate($request, [
            'nom_sous_categorie_boutique' => "required|unique:sous_categorie_boutiques,nom_sous_categorie_boutique,$cat_boutiques->id",
            'categorie_boutique_id' => 'required',
        ]);

        $cat_boutiques->nom_sous_categorie_boutique = $request->nom_sous_categorie_boutique;
        $cat_boutiques->slug_sous_categorie_boutique = Str::slug($request->nom_sous_categorie_boutique, '-');
        $cat_boutiques->nom_sous_categorie_boutique = $request->nom_sous_categorie_boutique;
        $cat_boutiques->save();

        Session::flash('success', 'Une sous Categorie vient d\'être modifié avec succès');

        return redirect()->route('sous-categorie-boutique.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat_boutiques = SousCategorieBoutique::find($id);
        $cat_boutiques->delete();

        Session::flash('success', 'La sous categorie a été supprimé avec succès');
        return redirect()->route('sous-categorie-boutique.index');
    }
}
