<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\User;

use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coun_all = User::all();
        $coun_ad = User::join('roles', 'users.role_id', 'roles.id')->where('roles.code_role',  'AD')->get();
        $coun_cl = User::join('roles', 'users.role_id', 'roles.id')->where('roles.code_role',  'CL')->get();

        $users = User::paginate(5);
        $admins = User::join('roles', 'users.role_id', 'roles.id')->where('roles.code_role',  'AD')
                        ->select('users.*')->paginate(5);
        $clients = User::join('roles', 'users.role_id', 'roles.id')->where('roles.code_role',  'CL')
                        ->select('users.*')->paginate(5);
        return view('Admin.users.index', compact('users', 'admins', 'clients', 'coun_all', 'coun_ad', 'coun_cl'));
    }

    /**
     * Show the admin form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_admin()
    {
        return view('Admin.users.create-admin');
    }

    /**
     * Show the customer form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_client()
    {
        return view('Admin.users.create-client');
    }

    /**
     * Store a newly admin created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_admin(Request $request)
    {
        $this->validate($request, [
            'prenom_user' => 'required',
            'nom_user' => 'required',
            'email' => 'required|unique:users,email',
            'adresse_user' => 'required',
            'telephone_user' => 'required',
        ]);

        User::create([
            'prenom_user' => $request->prenom_user,
            'nom_user' => $request->nom_user,
            'email' => $request->email,
            'adresse_user' => $request->adresse_user,
            'telephone_user' => $request->telephone_user,
            'role_id' => 1,
            'password' => Hash::make(Str::random(10)),
        ]);
        Session::flash('success', 'Un nouveau administrateur vient d\'être crée avec succès');

        return redirect()->route('utilisateur.index');
    }

    /**
     * Store a newly customer created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_client(Request $request)
    {
        $this->validate($request, [
            'prenom_user' => 'required',
            'nom_user' => 'required',
            'email' => 'required|unique:users,email',
            'adresse_user' => 'required',
            'telephone_user' => 'required',
        ]);

        User::create([
            'prenom_user' => $request->prenom_user,
            'nom_user' => $request->nom_user,
            'email' => $request->email,
            'adresse_user' => $request->adresse_user,
            'telephone_user' => $request->telephone_user,
            'role_id' => 2,
            'password' => Hash::make(Str::random(10)),
        ]);
        Session::flash('success', 'Un nouveau client vient d\'être crée avec succès');

        return redirect()->route('utilisateur.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_client($id)
    {
       $user = User::where('id', $id)->first();
        return view('Admin.users.edit-client', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_client(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        $this->validate($request, [
            'prenom_user' => 'required',
            'nom_user' => 'required',
            'email' => "required|unique:users,email,$user->id",
            'adresse_user' => 'required',
            'telephone_user' => 'required',
        ]);

        $user->prenom_user = $request->prenom_user;
        $user->nom_user = $request->nom_user;
        $user->adresse_user = $request->adresse_user;
        $user->telephone_user = $request->telephone_user;
        $user->save();
        Session::flash('success', 'Un Client vient d\'être modifié avec succès');

        return redirect()->route('utilisateur.index');
    }

    /**
     * Display user informations of the user auth's.
     *
     * @return \Illuminate\Http\Response
     */
    public function profil()
    {
        $user = auth()->user();
        $user_id = $user->id;

        $profil = User::join('roles', 'users.role_id', 'roles.id')->where('users.id', $user_id)->first();
        return view('Admin.profil', compact('profil'));
    }

    /**
     * Reset User password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_pwd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ancien_mot_de_passe' => 'required',
            'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[@$!%*#?&]).*$/|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect(route('utilisateur.profil'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = auth()->user();
        if(Hash::check($request->ancien_mot_de_passe, $user->password)){
            $user->fill([
                'password' => Hash::make($request->password),
            ])->save();

            Session::flash('success', 'Le mot de passe a été mis à jour');
            return redirect()->to(route('utilisateur.profil'));
        } else{
            Session::flash('warning', 'L\'ancien mot de passe ne correspond pas');
            return redirect()->to(route('utilisateur.profil'));
        }
    }

    /**
     * Update the auth user's profile information
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_auth_profile(Request $request)
    {
        $user = auth()->user();

        $this->validate($request, [
            'prenom_user' => 'required|string|max:255',
            'nom_user' => 'required|string|max:255',
            'email' => "required|email|unique:users,email, $user->id",
            'adresse_user' => 'required',
            'telephone_user' => 'required',
        ]);

        $user->prenom_user = $request->prenom_user;
        $user->nom_user = $request->nom_user;
        $user->email = $request->email;
        $user->adresse_user = $request->adresse_user;
        $user->telephone_user = $request->telephone_user;

        if($request->hasFile('photo')){
            $photo = $request->profile_photo_path;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/users/', $image_new_name);
            $user->profile_photo_path = 'public/storage/users/'.$image_new_name;
        }

        $user->save();
        Session::flash('success', 'Votre profil a été mise à jour avec succès');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_admin($id)
    {
       $user = User::where('id', $id)->first();
        return view('Admin.users.edit-admin', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_admin(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        $this->validate($request, [
            'prenom_user' => 'required',
            'nom_user' => 'required',
            'email' => "required|unique:users,email,$user->id",
            'adresse_user' => 'required',
            'telephone_user' => 'required',
        ]);

        $user->prenom_user = $request->prenom_user;
        $user->nom_user = $request->nom_user;
        $user->adresse_user = $request->adresse_user;
        $user->telephone_user = $request->telephone_user;
        $user->save();
        Session::flash('success', 'Un admin vient d\'être modifié avec succès');

        return redirect()->route('utilisateur.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        Session::flash('success', 'Un utilisateur a été supprimé avec succès');
        return redirect()->route('utilisateur.index');
    }
}
