<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategorieProduit;
use App\Models\Produit;
use App\Models\Boutique;

use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits = Produit::join('boutiques', 'produits.boutique_id', 'boutiques.id')
            ->join('categorie_produits', 'produits.categorie_produit_id', 'categorie_produits.id')
            ->select('produits.*', 'boutiques.nom_boutique', 'categorie_produits.nom_categorie_produit')
            ->paginate(10);

        return view('Admin.Produit.index', compact('produits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorieproduits = CategorieProduit::all();
        $boutiques = Boutique::all();
        return view('Admin.Produit.create', compact('categorieproduits', 'boutiques'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nom_produit' => 'required',
            'prix_produit' => 'required',
            'boutique_id' => 'required',
            'categorie_produit_id' => 'required',
            // 'auteur' => 'required',
        ]);

        $produit = Produit::create([
            'nom_produit' => $request->nom_produit,
            'slug_nom_produit' => Str::slug($request->nom_produit, '-'),
            'prix_produit' => $request->prix_produit,
            'boutique_id' => $request->boutique_id,
            'categorie_produit_id' => $request->categorie_produit_id,
            'description_produit' => $request->description_produit,
            'is_active_produit' => false,
        ]);

        if($request->hasFile('photo_produit')){
            $photo = $request->photo_produit;
            $image_new_name = rand() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/produits/', $image_new_name);
            $produit->photo_produit = '/public/storage/boutique/produits/'.$image_new_name;
        }

        $produit->save();

        Session::flash('success', 'Un nouveau Produit vient d\'être crée avec succès');

        return redirect()->route('produits.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produit = Produit::where('id', $id)->first();
        $boutiques = Boutique::all();
        $cat_produits = CategorieProduit::orderBy("nom_categorie_produit")->get();
        return view('Admin.Produit.edit', compact('produit', 'boutiques', 'cat_produits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produit = Produit::where('id', $id)->first();

        $this->validate($request, [
            'nom_produit' => 'required',
            'boutique_id' => 'required',
            'categorie_produit_id' => 'required',
            'prix_produit' => 'required',
        ]);

        $produit->nom_produit = $request->nom_produit;
        $produit->slug_nom_produit = Str::slug($request->nom_produit, '-');
        $produit->boutique_id = $request->boutique_id;
        $produit->categorie_produit_id = $request->categorie_produit_id;
        $produit->prix_produit = $request->prix_produit;
        $produit->description_produit = $request->description_produit;

        if($request->hasFile('photo_produit')){
            $photo = $request->photo_produit;
            $image_new_name = time() . '.' . $photo->getClientOriginalExtension();
            $photo->move('public/storage/boutique/produits/', $image_new_name);
            $produit->photo_produit = '/public/storage/boutique/produits/'.$image_new_name;
        }

        $produit->save();
        Session::flash('success', 'Votre produit vient d\'être modifié avec succès');

        return redirect()->route('produits.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produit = Produit::find($id);
        $produit->delete();

        Session::flash('success', 'un produit a été supprimé avec succès');
        return redirect()->route('produits.index');
    }
}
