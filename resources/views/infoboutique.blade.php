@extends('layouts.guest')
@section('content')
<header id="header"
    class="main-header header-float header-sticky header-sticky-smart header-light header-style-02 font-normal">
    <div class="header-wrapper sticky-area">
        <div class="container">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="/">
                        <img src="{{ asset('images/logo1.png') }}" alt="Boutique Senegal" />
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                        data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                        <i class="far fa-search"></i>
                    </a>
                </div>
                <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="/">
                        <img src="{{ asset('images/logo1.png') }}" alt="Boutique Senegal" />
                    </a>
                    <ul class="navbar-nav ml-auto bleuhov">
                        <li class="nav-item">
                            <a class="nav-link" href="/categorie">Catégories <span class="caret"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                        </li>
                        @auth
                            @if (Auth::user()->role_id == 1)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                </li>

                            @elseif(Auth::user()->role_id == 2)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                </li>

                            @endif
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                connecter<span class="caret"></i></span></a>
                        </li>
                        @endauth
                        <li class="nav-item ">
                            <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                Ajoutez votre boutique<span class="caret"></i></span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
    <br><br><br>

<div>
    <div class="image-gallery">
        <div class="slick-slider arrow-center"
            data-slick-options='{"slidesToShow": 1, "autoplay":true,"infinite": true,"centerMode": true,"centerPadding": "0","dots":false,"variableWidth": true, "variableHeight": true,"focusOnSelect": true,"responsive":[{"breakpoint": 576,"settings": {"arrows": false,"autoplay":true}}]}'>
            @foreach ($galleries as $gallerie)
                <div class="box"><img src="{{$gallerie->photo_gallerie}}" style="width: 450px; height:400px;" alt="Gallerie-{{$gallerie->id}}"></div>
            @endforeach
            @if ($galleries->count() == 0)
                <div class="box"><img src="{{asset('images/shop/bs.jpg')}}" style="width: 450px; height:400px;" alt="Boutique Senegal"></div>
            @endif
        </div>
    </div>
    <div class="page-title bg-gray-06 pt-8 pb-9">
        <div class="container">
            <ul class="breadcrumb breadcrumb-style-03 mb-6">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Accueil</a></li>
                <li class="breadcrumb-item"><a href="{{ route('guest.categorie-index', [$boutique->slug_categorie_boutique]) }}">{{$boutique->nom_categorie_boutique}} </a></li>
                <li class="breadcrumb-item">{{$boutique->nom_boutique}}</li>
            </ul>
            <div class="explore-details-top d-flex flex-wrap flex-lg-nowrap">
                <div class="store">
                    <div class="d-flex flex-wrap">
                        <h2 class="text-dark mr-3 mb-2">{{$boutique->nom_boutique}}
                        </h2>
                        <span class="check font-weight-semibold text-green mb-2">
                            <i class="fas fa-check-circle"></i>
                            Certifié
                        </span>
                    </div>
                    <ul class="list-inline store-meta d-flex flex-wrap align-items-center">
                        <li class="list-inline-item"><span
                                class="badge badge-success d-inline-block mr-1">5.0</span>
                            <span>4 notes</span>
                        </li>
                        <li class="list-inline-item separate"></li>
                        <li class="list-inline-item">
                            <a href="{{ route('guest.categorie-index', [$boutique->slug_categorie_boutique]) }}" class=" text-link text-decoration-none d-flex align-items-center">
                                <span class="d-inline-block mr-2 font-size-md">
                                {{-- <i class="fas fa-dumbbell"></i> --}}
                                </span>
                                <span>{{$boutique->categorie->nom_categorie_boutique}}</span>
                            </a>
                        </li>
                        <li class="list-inline-item separate"></li>
                        <li class="list-inline-item">
                            <span class="mr-1"><i class="fal fa-clock"></i></span>
                            <span>Posté il y a 15 heures</span>
                        </li>
                    </ul>
                </div>
                <div class="ml-0 ml-lg-auto mt-4 mt-lg-0 d-flex flex-wrap flex-sm-nowrap">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a href="#" class="btn btn-white font-size-md mb-3 mb-sm-0 py-1 px-3 rounded-sm">
                                <span class="d-inline-block mr-1">
                                    <i class="fal fa-share-alt"></i>
                                </span>
                                Partager</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="page-container">
            <div class="row">
                <div class="page-content col-xl-8 mb-8 mb-xl-0">
                    <div class="explore-details-container">
                        <div class="mb-9">
                            <h3
                                class="font-size-lg text-uppercase font-weight-semibold border-bottom pb-1 pt-2 mb-6">
                                description
                            </h3>
                            <div class="mb-7">
                                <p class="mb-6">
                                    {{$boutique->description_boutique}}
                                </p>
                            </div>
                        </div>
                        {{-- les produits --}}
                        @if ($produits->count() > 0)
                            <div class="block-more-listing mt-8 border-top pt-6">
                                <h4 class="font-size-md mb-5">Produits</h4>
                                <div class="slick-slider arrow-top store-grid-style" data-slick-options='{"slidesToShow": 3, "autoplay":false,"dots":false,"responsive":[{"breakpoint": 992,"settings": {"slidesToShow": 1}}]}'>
                                    @foreach ($produits as $produit)
                                        <div class="product card border-0 rounded-0 p-0">
                                            <div class="position-relative h-100">
                                                <a href="{{ route('guest.produit-index', [$boutique->slug_nom_boutique, $produit->slug_nom_produit]) }}">
                                                    <img src="{{$produit->photo_produit}}" style="height: 360px;"  alt="{{$produit->nom_produit}}"
                                                        class="card-img-top">
                                                </a>
                                            </div>
                                            <div class="card-body text-center position-relative">
                                                <a href="{{ route('guest.produit-index', [$boutique->slug_nom_boutique, $produit->slug_nom_produit]) }}"
                                                    class="link-hover-secondary-primary font-size-md mb-1">{{$produit->nom_produit}}</a>
                                                <div class="product-meta-wrapper position-relative">
                                                    <div class="">
                                                        <div class="font-size-md">
                                                            <span class="text-danger">{{$produit->prix_produit}} FCFA</span>
                                                        </div>
                                                    </div>
                                                    <div class="">
                                                        <a href="{{ route('guest.produit-index', [$boutique->slug_nom_boutique, $produit->slug_nom_produit]) }}"
                                                            class="link-hover-dark-primary font-weight-semibold text-uppercase">Contactez</a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <br>
                        <br>
                        <br>
                        @if ($like_shops->count() !== 0)
                            <div class="block-more-listing mt-8 border-top pt-6">
                                <h4 class="font-size-md mb-5">Plus de boutiques dans <span class="text-danger">{{$boutique->nom_categorie_boutique}}</span></h4>
                                <div class="slick-slider arrow-top store-grid-style"
                                    data-slick-options='{"slidesToShow": 2, "autoplay":false,"dots":false,"responsive":[{"breakpoint": 992,"settings": {"slidesToShow": 1}}]}'>
                                    @foreach ($like_shops as $shop_s)
                                        <div class="box">
                                            <div class="store card rounded-0 border-0">
                                                <div class="position-relative store-image">
                                                    <a href="{{ route('guest.boutique-index', [$shop_s->slug_nom_boutique]) }}">
                                                        <img src="{{$shop_s->photo_boutique}}" alt="{{$shop_s->slug_nom_boutique}}"
                                                            class="card-img-top rounded-0">
                                                    </a>
                                                    <div class="image-content position-absolute d-flex align-items-center">
                                                        <div class="content-right ml-auto d-flex">
                                                            <a href="{{$shop_s->photo_boutique}}"
                                                                class="item viewing" data-toggle="tooltip"
                                                                data-placement="top" title="Quick view"
                                                                data-gtf-mfp="true">
                                                                <svg class="icon icon-expand">
                                                                    <use xlink:href="#icon-expand"></use>
                                                                </svg>
                                                            </a>
                                                            <a href="#" class="item marking" data-toggle="tooltip"
                                                                data-placement="top" title="Bookmark"><i
                                                                    class="fal fa-bookmark"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body pb-4 border-right border-left">
                                                    <a href="{{ route('guest.boutique-index', [$shop_s->slug_nom_boutique]) }}"
                                                        class="card-title h5 text-dark d-inline-block mb-2">
                                                        <span class="letter-spacing-25">{{$shop_s->nom_boutique}}</span>
                                                    </a>
                                                    <ul
                                                        class="list-inline store-meta mb-2 font-size-sm d-flex align-items-center flex-wrap">
                                                        <li class="list-inline-item"><span class="mr-1">Catégorie : </span><span
                                                                class=" font-weight-semibold" style="color:#39d1ff">{{$shop_s->nom_categorie_boutique}}</span></li>
                                                        <li class="list-inline-item separate"></li>
                                                        <li class="list-inline-item"><span class="text-green">Ouverte</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="sidebar col-xl-4">
                    <div class="widget map mb-6 position-relative mb-6 rounded-0">
                        <div class="card p-4 widget border-0 infomation pt-0 bg-gray-06">
                            <div class="card-body px-0 py-2">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                        <span class="item-icon mr-3"><i
                                                class="fal fa-map-marker-alt"></i></span>
                                        <span class="card-text">{{$boutique->adresse_boutique}}</span>
                                    </li>
                                    <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                        <span class="item-icon mr-3"><i class="fas fa-phone"></i></span>
                                        <span class="card-text"><a href="tel:{{$boutique->telephone_boutique}}">{{$boutique->telephone_boutique}}</a></span>
                                    </li>
                                    <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                        <span class="item-icon mr-3"><i class="fab fa-whatsapp"></i></span>
                                        <span class="card-text"><a href="https://api.whatsapp.com/send?phone={{$boutique->telephone_boutique}}" >Whatsapp</a></span>
                                    </li>
                                    <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                        <span class="item-icon mr-3"><i class="fa fa-globe"></i></span>
                                        <span class="card-text"><a href="{{$boutique->site_web_boutique}}" target="_blank">{{$boutique->site_web_boutique}}</a></span>
                                    </li>
                                    @if ($boutique->map_url_boutique !== null)
                                        <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                            <span class="item-icon mr-3"><i class="fal fa-globe"></i></span>
                                            <span class="card-text"><a target="_blank" href="{{$boutique->map_url_boutique}}">{{$boutique->map_url_boutique}}</a></span>
                                        </li>
                                    @endif
                                    @if ($boutique->link_instagram_boutique !== null)
                                        <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                            <span class="item-icon mr-3"><i class="fab fa-instagram"></i></span>
                                            <span class="card-text"><a target="_blank" href="{{$boutique->link_instagram_boutique}}">{{$boutique->nom_boutique}}</a></span>
                                        </li>
                                    @endif
                                    @if ($boutique->link_facebook_boutique !== null)
                                        <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                            <span class="item-icon mr-3"><i class="fab fa-facebook"></i></span>
                                            <span class="card-text"><a target="_blank" href="{{$boutique->link_facebook_boutique}}">{{$boutique->nom_boutique}}</a></span>
                                        </li>
                                    @endif
                                    <li class="list-group-item bg-transparent d-flex text-dark px-0 pt-4">
                                        <div class="social-icon origin-color si-square text-center">
                                            <ul class="list-inline">
                                                <li class="list-inline-item si-facebook">
                                                    <a target="_blank" title="Facebook" href="{{$boutique->link_facebook_boutique}}">
                                                        <i class="fab fa-facebook-f">
                                                        </i>
                                                        <span>Facebook</span>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item si-twitter">
                                                    <a target="_blank" title="Instagram" href="{{$boutique->link_instagram_boutique}}">
                                                        <i class="fab fa-instagram">
                                                        </i>
                                                        <span>Instagram</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if ($galleries->count() > 0)
                        <div class="card p-4 widget border-0 gallery bg-gray-06 rounded-0 mb-6">
                            <div class="card-title d-flex mb-0 font-size-md font-weight-semibold text-dark text-uppercase border-bottom pb-2 lh-1">
                                <span class="text-secondary mr-3">
                                    <svg class="icon icon-ionicons_svg_md-images">
                                        <use xlink:href="#icon-ionicons_svg_md-images"></use>
                                    </svg>
                                </span>
                                <span>GALERIE DE PHOTOS</span>
                            </div>
                            <div class="card-body px-0 pt-6 pb-3">
                                <div class="photo-gallery d-flex flex-wrap justify-content-between">
                                    @foreach ($galleries as $gallerie)
                                        <a href="{{$gallerie->photo_gallerie}}" class="photo-item"
                                            data-gtf-mfp="true" data-gallery-id="01">
                                            <img src="{{$gallerie->photo_gallerie}}" alt="Gallerie {{$gallerie->id}}"></a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="recent-view pt-7 bg-white pb-10">
        <div class="container">
            <div class="mb-6">
                <h5 class="mb-0">
                    Vu récemment
                </h5>
            </div>
            <div class="row">
                @foreach ($shop_rs as $shop)
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="store media align-items-stretch bg-white">
                            <div class="store-image position-relative">
                                <a href="{{ route('guest.boutique-index', [$shop->slug_nom_boutique]) }}">
                                    <img src="{{$shop->photo_boutique}}" alt="{{$shop->nom_boutique}}">
                                </a>
                                <div class="image-content position-absolute d-flex align-items-center">
                                    <div class="content-right ml-auto d-flex">
                                        <a href="{{$shop->photo_boutique}}" class="{{$shop->nom_boutique}}"
                                            data-toggle="tooltip" data-placement="top" title="Quickview"
                                            data-gtf-mfp="true">
                                            <svg class="icon icon-expand">
                                                <use xlink:href="#icon-expand"></use>
                                            </svg>
                                        </a>
                                        <a href="#" class="item marking" data-toggle="tooltip" data-placement="top"
                                            title="Bookmark"><i class="fal fa-bookmark"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="media-body pl-0 pl-sm-3 pt-4 pt-sm-0">
                                <a href="{{ route('guest.boutique-index', [$shop->slug_nom_boutique]) }}"
                                    class="font-size-md font-weight-semibold text-dark d-inline-block mb-2 lh-11"><span
                                        class="letter-spacing-25">{{$shop->nom_boutique}}</span> </a>
                                <ul
                                    class="list-inline store-meta mb-2 lh-1 font-size-sm d-flex align-items-center flex-wrap">
                                    <li class="list-inline-item">
                                        <li class="list-inline-item">
                                            <span class="mr-1">Categorie: </span>
                                            <span class="text-danger font-weight-semibold">{{$shop->categorie->nom_categorie_boutique}}</span>
                                    </li>
                                    <li class="list-inline-item separate"></li>
                                    <li class="list-inline-item mt-1">
                                        <span class="text-green">Ouvert !</span>
                                    </li>
                                </ul>
                                <div>
                                    <span class="d-inline-block mr-1"><i class="fal fa-map-marker-alt">
                                        </i>
                                    </span>
                                    <a href="#" class="text-secondary text-decoration-none address">
                                        {{$shop->adresse_boutique}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
    <style>
        .navbar {
            overflow: hidden;
            background-color: #333;
            position: fixed !important; /* Set the navbar to fixed position */
            top: 0; /* Position the navbar at the top of the page */
            width: 100%; /* Full width */
        }
    </style>
@endsection
