@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Admin.Layouts.sidebar')

        <div class="page-container">
            <div class="container-fluid h-100">
                <div class="page-content-wrapper d-flex flex-column h-100">
                    <h1 class="font-size-h4 mb-4 font-weight-bold">Article</h1>
                    <div class="page-content">
                        <div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
                            <div class="card-header p-0 bg-transparent">
                                <h5 class="card-title text-capitalize">Nouveau Article</h5>
                            </div>
                            <div class="card-body px-0 pt-4 pb-0">
                                <div class="form-update-profile">
                                    <form method="POST" action="{{ route('article.store') }}"  enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group mb-2">
                                            <label for="titre_article" class="font-size-md text-dark font-weight-semibold mb-1">Titre <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control @error('titre_article') border border-danger @enderror" id="titre_article" name="titre_article" placeholder="titre_article">
                                            @error('titre_article')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-2">
                                                    <label for="categorie_id" class="font-size-md text-dark font-weight-semibold mb-1">Categorie <span class="text-danger">*</span></label>
                                                    <select id="categorie_id" name="categorie_id" class="form-control color-gray @error('categorie_id') border border-danger @enderror">
                                                            <option selected>Choisir la categorie</option>
                                                            @foreach ($categories as $categorie)
                                                                <option value="{{$categorie->id}}">{{$categorie->nom_categorie_blog}}</option>
                                                            @endforeach
                                                    </select>
                                                    @error('categorie_id')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                        <div class="form-group mb-2">
                                            <label for="tags" class="font-size-md text-dark font-weight-semibold mb-1">Etiquette</label>
                                            <select class="selectpicker form-control" name="tags[]" id="tags" title="Choisissez les etiquettes..." data-live-search="true" data-hide-disabled="true" data-actions-box="true" multiple>
                                                    @foreach ($tags as $tag)
                                                        <option value="{{$tag->id}}">{{$tag->libelle}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                            </div>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="photo_article" class="font-size-md text-dark font-weight-semibold mb-1">Photo</label>
                                            <input type="file" class="form-control" id="photo_article" name="photo_article" placeholder="photo_article">
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="description_article" class="font-size-md text-dark font-weight-semibold mb-1">description <span class="text-danger">*</span></label>
                                            <textarea class="form-control @error('description_article') border border-danger @enderror" name="description_article" id="description_article"></textarea>
                                            @error('description_article')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block font-size-lg">AJOUTER</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-auto pt-6">&copy; 2020 Boutique Senegal. All Rights Reserved.</div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('style')
    <!-- Bootstrap Select css -->
    {{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <!-- Bootstrap Select css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" />

    <!-- summernote css -->
@endsection
@section('script')
    <!-- Bootstrap Select js-->
    <!-- Bootstrap Select js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    
    <!-- Summernote js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote.min.js" integrity="sha512-kZv5Zq4Cj/9aTpjyYFrt7CmyTUlvBday8NGjD9MxJyOY/f2UfRYluKsFzek26XWQaiAp7SZ0ekE7ooL9IYMM2A==" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#description_article').summernote({
                placeholder: 'Definir le contenu',
                tabsize: 2,
                height: 200
            });
        });
    </script>
@endsection
@endsection

