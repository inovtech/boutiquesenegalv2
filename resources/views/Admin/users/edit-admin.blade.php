@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Admin.Layouts.sidebar')

        <div class="page-container">
            <div class="container-fluid h-100">
                <div class="page-content-wrapper d-flex flex-column h-100">
                    <h1 class="font-size-h4 mb-4 font-weight-normal">Utilisateur</h1>
                    <div class="page-content">
                        <div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
                            <div class="card-header p-0 bg-transparent">
                                <h5 class="card-title text-capitalize">Modifier Administrateur - {{$user->prenom_user}}</h5>
                            </div>
                            <div class="card-body px-0 pt-4 pb-0">
                                <div class="form-update-profile">
                                    <form method="POST" action="{{ route('utilisateur.edit-admin', [$user->id]) }}">
                                        @method('PUT')
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-2">
                                                    <label for="prenom_user" class="font-size-md text-dark font-weight-semibold mb-1">Prenom</label>
                                                    <input type="text" class="form-control" id="prenom_user" value="{{$user->prenom_user}}" name="prenom_user" placeholder="Prenom">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-2">
                                                    <label for="nom_user" class="font-size-md text-dark font-weight-semibold mb-1">Nom</label>
                                                    <input type="text" class="form-control" id="nom_user" value="{{$user->nom_user}}" name="nom_user" placeholder="Nom">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group mb-2">
                                                    <label for="email" class="font-size-md text-dark font-weight-semibold mb-1">Email</label>
                                                    <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" placeholder="Email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-2">
                                                    <label for="adresse_user" class="font-size-md text-dark font-weight-semibold mb-1">Adresse</label>
                                                    <input type="text" class="form-control" id="adresse_user" value="{{$user->adresse_user}}" name="adresse_user" placeholder="Adresse">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-2">
                                                    <label for="telephone_user" class="font-size-md text-dark font-weight-semibold mb-1">Telephone</label>
                                                    <input type="tel" class="form-control" id="telephone_user" value="{{$user->telephone_user}}" name="telephone_user" placeholder="Telephone">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block font-size-lg">MODIFIER</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-auto pt-6">&copy; 2020 Thedir. All Rights Reserved.</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
