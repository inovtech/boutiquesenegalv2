@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Admin.Layouts.sidebar')

        <div class="page-container">
            <div class="container-fluid h-100">
                <div class="page-content-wrapper d-flex flex-column h-100">
                    <h1 class="font-size-h4 mb-4 font-weight-normal">Tag</h1>
                    <div class="page-content">
                        <div class="font-size-lg text-dark font-weight-semibold border-bottom mb-3 pb-2 lh-1">
                            <div class="row">
                                <div class="col-md-9">Listes Tag</div>
                                <div class="col-md-3">
                                    <a href="/admin/tag/create" class="btn btn-info">Nouveau tag</a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive-md">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">ID</th>
                                        <th style="width: 12%">LIBELLE</th>
                                        <th style="width: 10%">SLUG</th>
                                        <th style="width: 10%">DESCRIPTION</th>
                                        <th style="width: 10%">ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($Tags as $Tag)
                                        <tr>
                                            <td><a href="panel-invoice-details.html" class="link-hover-dark-blue">CATB-00{{$Tag->id}}</a>
                                            </td>
                                            <td>{{$Tag->libelle}}</td>
                                            <td>{{$Tag->slug}}</td>
                                            <td>{{$Tag->description}}</td>
                                            <td>
                                                <a href="{{ route('tag.edit', [$Tag->id]) }}" class="btn btn-info btn-icon mb-2 mb-sm-0 font-size-md">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="{{ route('tag.destroy', [$Tag->id]) }}" class="btn btn-danger btn-icon mb-2 mb-sm-0 font-size-md" data-toggle="modal" onclick="deleteData( {{ $Tag->id }} )" data-target="#modalDeletePack">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{$Tags->links()}}
                        </div>
                    </div>
                    <div class="mt-auto pt-6">&copy; 2020 Thedir. All Rights Reserved.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
    <div class="modal fade" id="modalDeletePack" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header bg-light">
                    <h5 class="text-uppercase" id="exampleModalLongTitle"><i class="mdi mdi-account-circle mr-1"></i>
                            Suppression Pack Abonnement
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="deleteForm" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                <div class="modal-body">
                        <p class="text-center" style="font-size:16px">Etes-vous sûr de vouloir supprimer ce pack ?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger mr-1" data-dismiss="modal" onclick="formSubmit()">Supprimer</button>
                    <button type="close" class="btn btn-success" data-dismiss="modal" >Annuler</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('script')
    <script type=text/javascript>
        function deleteData(id)
        {
            var id = id;
            var url = '{{ route("tag.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit()
        {
            $("#deleteForm").submit();
        }
    </script>
@endsection

@endsection
