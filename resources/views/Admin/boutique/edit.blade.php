@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Admin.Layouts.sidebar')

        <div class="page-container">
            <div class="container-fluid h-100">
                <div class="page-content-wrapper d-flex flex-column h-100">
                    <h1 class="font-size-h4 mb-4 font-weight-normal">Boutique</h1>
                    <div class="page-content">
                        <div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
                            <div class="card-header p-0 bg-transparent">
                                <h5 class="card-title text-capitalize">Modifier {{$boutique->nom_boutique}} Boutique</h5>
                            </div>

                            <div id="submit-listing" class="section-submit-listing pb-2">
                                <form method="POST" action="{{ route('boutique.update', [$boutique->id]) }}" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    <div class="submit-listing-blocks mb-9 border-bottom pb-6">
                                        <div class="row lh-18">
                                            <div class="col-md-6 ">
                                                <div class="card border-0 p-0">
                                                    <img src="{{asset('images/other/submit-listing-1.jpg')}}" alt="Submit listing 01"
                                                        class="card-img-top">
                                                    <div class="card-body px-0 pt-6">
                                                        <div class="form-group mb-4">
                                                            <div class="text-dark font-weight-semibold font-size-md mb-2 lh-15">
                                                                Nom Boutique
                                                                <span class="text-danger">*</span>
                                                            </div>
                                                            <input type="text" id="nom_boutique" name="nom_boutique" value="{{$boutique->nom_boutique}}" class="form-control" placeholder="Nom Boutique" required>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <div class="mb-2 d-flex align-items-center lh-15">
                                                                <label
                                                                    class="mb-0 text-dark font-weight-semibold font-size-md lh-15"
                                                                    for="ville_boutique">VIlle</label>
                                                                    <span class="text-danger"> *</span>
                                                            </div>
                                                            <input type="text" id="ville_boutique" name="ville_boutique" value="{{$boutique->ville_boutique}}" class="form-control" placeholder="Dakar" required>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="telephone_boutique"
                                                                class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Telephone
                                                                <span class="text-danger">*</span></label>
                                                            <input type="tel" id="telephone_boutique" name="telephone_boutique" value="{{$boutique->telephone_boutique}}" class="form-control" placeholder="+221 77 777 77 77" required>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="site_web_boutique"
                                                                class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Site Web</label>
                                                            <input type="text" id="site_web_boutique" name="site_web_boutique" value="{{$boutique->site_web_boutique}}" class="form-control" placeholder="http://">
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="categorie_boutique_id"
                                                                class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Categorie
                                                                <span class="text-danger"> *</span></label>
                                                            <select id="categorie_boutique_id" name="categorie_boutique_id" class="form-control color-gray" required>
                                                                <option disabled>Chosir votre categorie de boutique </option>
                                                                @foreach ($categories as $categorie)
                                                                    <option value="{{$categorie->id}}" @if ($boutique->categorie_boutique_id == $categorie->id)
                                                                        selected
                                                                    @endif>{{$categorie->nom_categorie_boutique}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <div class="mb-2 d-flex align-items-center lh-15">
                                                                <label class="mb-0 text-dark font-weight-semibold font-size-md lh-15"
                                                                    for="adresse_boutique">Localisation Siege boutique
                                                                    <span class="text-danger"> *</span></label>
                                                            </div>
                                                            <input type="text" id="adresse_boutique" name="adresse_boutique" class="form-control"
                                                                placeholder="adresse ville" value="{{$boutique->adresse_boutique}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="card border-0 p-0">
                                                    <img src="{{asset('images/other/submit-listing-2.jpg')}}" alt="Submit listing 02"
                                                        class="card-img-top">
                                                    <div class="card-body px-0 pt-6">
                                                        <div class="form-group business-hours mb-2">
                                                            <div class="text-dark font-weight-semibold font-size-md mb-3 lh-12">Horaire de travail
                                                            </div>
                                                            <div class="row lh-18">
                                                                <span class="col-2 col-md-2 "></span>
                                                                <span class="font-weight-semibold col-4 col-md-3">Lundi</span>
                                                                <a href="#" class="col-2 col-md-3 text-danger text-decoration-none">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="form-row align-items-center form-time">
                                                            <div class="col item form-group day">
                                                                <select class="form-control">
                                                                    <option>Tous les jours</option>
                                                                    <option>Lundi</option>
                                                                    <option>Mardi</option>
                                                                    <option>Mercredi</option>
                                                                    <option>Jeudi</option>
                                                                    <option>Vendredi</option>
                                                                    <option>Samedi</option>
                                                                    <option>Dimanche</option>
                                                                </select>
                                                            </div>
                                                            <div class="col item">
                                                                <a href="#"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="user_boutique_id"
                                                                class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Client
                                                                <span class="text-danger"> *</span></label>
                                                            <select id="user_boutique_id" name="user_boutique_id" class="form-control color-gray">
                                                                <option disabled>A qui appartient la boutique ? </option>
                                                                @foreach ($users as $user)
                                                                    <option value="{{$user->id}}" @if ($boutique->user_boutique_id == $user->id)
                                                                        selected
                                                                    @endif>{{$user->prenom_user}} {{$user->nom_user}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <div class="mb-2 d-flex align-items-center lh-15">
                                                                <label
                                                                    class="mb-0 text-dark font-weight-semibold font-size-md lh-15"
                                                                    for="map_url_boutique">Adresse Map (Geolocation)</label>
                                                            </div>
                                                            <input type="text" id="map_url_boutique" name="map_url_boutique" value="{{$boutique->map_url_boutique}}" class="form-control"
                                                                placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="submit-listing-blocks mb-6">
                                        <div class="row lh-18">
                                            <div class="col-md-6 submit-listing-block">
                                                <div class="card border-0 p-0">
                                                    <img src="{{asset('images/other/submit-listing-3.jpg')}}" alt="Submit listing 02"
                                                        class="card-img-top">
                                                    <div class="card-body px-0 pt-6">
                                                        <div class="form-group mb-4">
                                                            <label class="text-dark font-weight-semibold font-size-md mb-2 lh-15"
                                                                for="description_boutique">Description<span class="text-danger"> *</span>
                                                            </label>
                                                            <textarea id="description_boutique" name="description_boutique" class="form-control" data-tinymce="true"
                                                                placeholder="Decrivez-nous ce que fait votre boutique" required>{{$boutique->description_boutique}}</textarea>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label
                                                                class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Liens Reseaux Sociaux</label>
                                                            <div class="row mb-4">
                                                                <div class="col-md-12 mb-4 mb-md-0">
                                                                    <input type="url" class="form-control"
                                                                        name="link_twitter_boutique" value="{{$boutique->link_twitter_boutique}}" placeholder="https://www.twitter.com/boutique_senegal/">
                                                                </div>
                                                            </div>
                                                            <div class="row mb-4">
                                                                <div class="col-md-12 mb-4 mb-md-0">
                                                                    <div class="col-md-12">
                                                                        <input type="url" class="form-control" value="{{$boutique->link_facebook_boutique}}"
                                                                            name="link_facebook_boutique" placeholder="https://www.facebook.com/boutique_senegal/">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row mb-4">
                                                                <div class="col-md-12 mb-4 mb-md-0">
                                                                    <input type="url" class="form-control" value="{{$boutique->link_youtube_boutique}}"
                                                                        name="link_youtube_boutique" placeholder="https://www.youtube.com/boutique_senegal/">
                                                                </div>
                                                            </div>
                                                            <div class="row mb-4">
                                                                <div class="col-md-12 mb-4 mb-md-0">
                                                                    <div class="col-md-12">
                                                                        <input type="url" class="form-control" value="{{$boutique->link_instagram_boutique}}"
                                                                            name="link_instagram_boutique" placeholder="https://www.instagram.com/boutique_senegal/">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 submit-listing-block">
                                                <div class="card border-0 p-0">
                                                    <img src="{{asset('images/other/submit-listing-4.jpg')}}" alt="Submit listing 04"
                                                        class="card-img-top">
                                                    <div class="card-body px-0 pt-6">
                                                        <div class="form-group mb-4">
                                                            <label
                                                                class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Logo de la boutique
                                                                <span class="text-danger"> *</span>
                                                            </label>
                                                            <input type="file" class="form-control" name="photo_boutique" id="photo_boutique">
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label
                                                                class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Gallerie de Produits
                                                            </label>
                                                            <fieldset class="form-group">
                                                                <a class="btn btn-primary font-size-md px-5" href="javascript:void(0)" onclick="$('#pro-image').click()">
                                                                    Choisir Photo(s)
                                                                </a>
                                                                <input type="file" id="pro-image" name="galleries[]" style="display: none;" class="form-control" multiple="true">
                                                            </fieldset>
                                                            <div class="preview-images-zone">
                                                                @foreach($gallerieImages as $image)
                                                                    <div class="preview-image preview-show-{{ $image->id }}">
                                                                        <div class="image-cancel" data-no="{{ $image->id }}">x</div>
                                                                        <div class="image-zone"><img id="pro-img-{{ $image->id }}" src="{{ asset($image->photo_gallerie) }}"></div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pt-8 border-top">
                                        <div class="contact-section text-center pt-3">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group mb-5">
                                                        <label for="email_boutique" class="font-weight-semibold text-dark font-size-md">Adresse Email <span class="text-danger"> *</span></label>
                                                        <input type="email" class="form-control" id="email_boutique" name="email_boutique"
                                                            placeholder="votre adresse email" value="{{$boutique->email_boutique}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-5">
                                                        <label for="pack_boutique_id" class="font-weight-semibold text-dark font-size-md">Pack Abonnement <span class="text-danger"> *</span></label>
                                                        <select id="pack_boutique_id" name="pack_boutique_id" class="form-control color-gray">
                                                            <option disabled>Chosir votre pack abonnement </option>
                                                            @foreach ($packs as $pack)
                                                                <option value="{{$pack->id}}" @if ($boutique->pack_boutique_id == $pack->id)
                                                                    selected
                                                                @endif>{{$pack->nom_pack_abonnement}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block font-size-h5 lh-19 mt-2">Enregistrer</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="mt-auto pt-6">&copy; 2020 Thedir. All Rights Reserved.</div>
                </div>
            </div>
        </div>
    </div>
</div>



    @section('style')
        <!------ Include the above in your HEAD tag ---------->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <style>
            .preview-images-zone {
                width: 100%;
                border: 1px solid #ddd;
                min-height: 180px;
                /* display: flex; */
                padding: 5px 5px 0px 5px;
                position: relative;
                overflow:auto;
            }
            .preview-images-zone > .preview-image:first-child {
                height: 185px;
                width: 185px;
                position: relative;
                margin-right: 5px;
            }
            .preview-images-zone > .preview-image {
                height: 90px;
                width: 90px;
                position: relative;
                margin-right: 5px;
                float: left;
                margin-bottom: 5px;
            }
            .preview-images-zone > .preview-image > .image-zone {
                width: 100%;
                height: 100%;
            }
            .preview-images-zone > .preview-image > .image-zone > img {
                width: 100%;
                height: 100%;
            }
            .preview-images-zone > .preview-image > .tools-edit-image {
                position: absolute;
                z-index: 100;
                color: #fff;
                bottom: 0;
                width: 100%;
                text-align: center;
                margin-bottom: 10px;
                display: none;
            }
            .preview-images-zone > .preview-image > .image-cancel {
                font-size: 18px;
                position: absolute;
                top: 0;
                right: 0;
                font-weight: bold;
                margin-right: 10px;
                cursor: pointer;
                display: none;
                z-index: 100;
            }
            .preview-image:hover > .image-zone {
                cursor: move;
                opacity: .5;
            }
            .preview-image:hover > .tools-edit-image,
            .preview-image:hover > .image-cancel {
                display: block;
            }
            .ui-sortable-helper {
                width: 90px !important;
                height: 90px !important;
            }

            .container {
                padding-top: 50px;
            }
        </style>
    @endsection

    @section('script')
        <script src="{{asset('vendors/tinymce/js/tinymce/tinymce.min.js')}}"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                document.getElementById('pro-image').addEventListener('change', readImage, false);

                $( ".preview-images-zone" ).sortable();

            });

            var num = 4;
            function readImage() {
                if (window.File && window.FileList && window.FileReader) {
                    var files = event.target.files; //FileList object
                    var output = $(".preview-images-zone");

                    for (let i = 0; i < files.length; i++) {
                        var file = files[i];
                        if (!file.type.match('image')) continue;

                        var picReader = new FileReader();

                        picReader.addEventListener('load', function (event) {
                            var picFile = event.target;
                            var html =  '<div class="preview-image preview-show-' + num + '">' +
                                        '<div class="image-cancel" data-no="' + num + '">x</div>' +
                                        '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
                                        '</div>';

                            output.append(html);
                            num = num + 1;
                        });

                        picReader.readAsDataURL(file);
                    }
                    $("#pro-image").val('galleries[]');
                } else {
                    console.log('Browser not support');
                }
            }

        </script>
        <script>
            $(document).on('click', '.image-cancel', function() {
                let no = $(this).data('no');
                console.log('clicked');

                $.ajax ({
                    type : 'get',
                    url: '{!!URL::to('/admin/removeImageFromGallerie')!!}',
                    data : { 'id' : no },
                    success : function(data){
                        console.log(" .preview-image.preview-show-"+no + " removed");
                        $(".preview-image.preview-show-"+no).remove();
                    },
                    error : function(){
                        console.log("An error occured !");
                    }
                });

            });
        </script>
    @endsection

@endsection
