@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/logo.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Admin.Layouts.sidebar')

        <div class="page-container">
            <div class="container-fluid h-100">
                <div class="page-content-wrapper d-flex flex-column h-100">
                    <h1 class="font-size-h4 mb-4 font-weight-normal">Boutique</h1>
                    <div class="page-content">
                        <div class="font-size-lg text-dark font-weight-semibold border-bottom mb-3 pb-2 lh-1">
                            <div class="row">
                                <div class="col-md-9">Listes Boutique</div>
                                <div class="col-md-3">
                                    <a href="/admin/boutique/create" class="btn btn-info">Nouvelle Boutique</a>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive-md">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">LOGO</th>
                                        <th style="width: 12%">NOM</th>
                                        <th style="width: 10%">TELEPHONE</th>
                                        <th style="width: 10%">CATEGORIE</th>
                                        <th style="width: 10%">OFFRE</th>
                                        <th style="width: 10%">STATUT</th>
                                        <th style="width: 10%">ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($boutiques as $boutique)
                                        <tr>
                                            <td>
                                                <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank" rel="noopener noreferrer">
                                                    <img src="{{$boutique->photo_boutique}}" alt="{{$boutique->nom_boutique}}" style="width: 60px;">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" target="_blank" rel="noopener noreferrer">{{$boutique->nom_boutique}}</a>
                                            </td>
                                            <td>{{$boutique->telephone_boutique}}</td>
                                            <td>{{$boutique->categorie->nom_categorie_boutique}}</td>
                                            <td>{{$boutique->pack->nom_pack_abonnement}}</td>
                                            <td>
                                                <button class="btn btn-xs @if($boutique->is_active_boutique == 0) btn-danger @elseif($boutique->is_active_boutique == 1) btn-success @endif text-uppercase text-black">
                                                    @if($boutique->is_active_boutique == 0)
                                                        Désactivé
                                                    @elseif($boutique->is_active_boutique == 1)
                                                        Activé
                                                    @endif
                                                </button>
                                            </td>
                                            <td>
                                                <a href="{{ route('boutique.edit', [$boutique->id]) }}" class="btn btn-info btn-icon mb-2 mb-sm-0 font-size-md">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="{{ route('boutique.destroy', [$boutique->id]) }}" class="btn btn-danger btn-icon mb-2 mb-sm-0 font-size-md" data-toggle="modal" onclick="deleteData( {{ $boutique->id }} )" data-target="#modalDeletePack">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <button type="button" class="btn btn-xs @if($boutique->is_active_boutique == 0) btn-success @elseif($boutique->is_active_boutique == 1) btn-warning text-white @endif" data-toggle="modal" data-target="#boutiqueStatut{{$boutique->id}}">
                                                    <i class="mdi @if($boutique->is_active_boutique == 0) fa fa-check @elseif($boutique->is_active_boutique == 1) fa fa-window-close @endif"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{$boutiques->links()}}
                        </div>
                    </div>
                    <div class="mt-auto pt-6">&copy; 2020 Thedir. All Rights Reserved.</div>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach($boutiques as $boutique)
<!-- Modal Update Statut -->
<div class="modal fade" id="boutiqueStatut{{$boutique->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h5 class="text-uppercase" id="updateModalLongTitle">
                    @if($boutique->is_active_boutique == 0)
                        Activation boutique
                    @elseif($boutique->is_active_boutique == 1)
                        Désactivation boutique
                    @endif
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('boutique.statut') }}" method="post">
                @csrf
                @method('GET')
                <div class="modal-body" >
                    <input type="hidden" name="id" value="{{$boutique->id}}">
                    <input type="hidden" name="is_active_boutique" value="{{$boutique->is_active_boutique}}">
                    <p class="text-center" id="updateModalLongBody" style="font-size:16px">
                        @if($boutique->is_active_boutique == 0)
                            Etes-vous sûr de vouloir activer cette boutique ?
                        @elseif($boutique->is_active_boutique == 1)
                            Etes-vous sûr de vouloir désactiver cette boutique ?
                        @endif
                    </p>
                </div>
                <div class="modal-footer">
                    @if($boutique->is_active_boutique == 0)
                        <button type="submit" class="btn btn-success mr-1">Activer</button>
                    @elseif($boutique->is_active_boutique == 1)
                        <button type="submit" class="btn btn-danger mr-1">Désactiver</button>
                    @endif
                    <button type="close" class="btn btn-secondary" data-dismiss="modal" >Annuler</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endforeach

<!-- Modal -->
    <div class="modal fade" id="modalDeletePack" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header bg-light">
                    <h5 class="text-uppercase" id="exampleModalLongTitle"><i class="mdi mdi-account-circle mr-1"></i>
                            Suppression Boutique
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" id="deleteForm" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                <div class="modal-body">
                        <p class="text-center" style="font-size:16px">Etes-vous sûr de vouloir supprimer cette Boutique ?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger mr-1" data-dismiss="modal" onclick="formSubmit()">Supprimer</button>
                    <button type="close" class="btn btn-success" data-dismiss="modal" >Annuler</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @section('script')
    <script type=text/javascript>
        function deleteData(id)
        {
            var id = id;
            var url = '{{ route("boutique.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit()
        {
            $("#deleteForm").submit();
        }
    </script>
@endsection

@endsection
