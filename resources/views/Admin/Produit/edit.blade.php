@extends('layouts.app')
@section('content')
    <header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
        <div class="header-wrapper sticky-area border-bottom">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                            <img src="{{ asset('images/Logos.png') }}" alt="TheDir">
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                            data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i
                                class="far fa-search"></i></a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                            <img src="{{ asset('images/Logos.png') }}" alt="TheDir">
                        </a>
                    </div>
                </nav>
            </div>
        </div>
    </header>


    <div id="wrapper-content" class="wrapper-content pt-0 pb-0">
        <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
            @include('Admin.Layouts.sidebar')

            <div class="page-container">
                <div class="container-fluid h-100">
                    <div class="page-content-wrapper d-flex flex-column h-100">
                        <h1 class="font-size-h4 mb-4 font-weight-bold">Produit</h1>
                        <div class="page-content">
                            <div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
                                <div class="card-header p-0 bg-transparent">
                                    <h5 class="card-title text-capitalize">Modifier Produit {{$produit->nom_produit}}</h5>
                                </div>

                                <div id="submit-listing" class="section-submit-listing pb-2">
                                    <form method="POST" action="{{ route('produits.update', [$produit->id]) }}" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group mb-2">
                                                <input type="text" hidden value="{{$produit->id}}" name='id'>
                                            <label for="nom_produit" class="font-size-md text-dark font-weight-semibold mb-1">Nom Produit
                                                <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control  @error('nom_produit') border border-danger @enderror" value="{{$produit->nom_produit}}" id="nom_produit" name="nom_produit" placeholder="Nom Produit">
                                            @error('nom_produit')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="categorie_produit_id" class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Categorie Produit
                                                <span class="text-danger">*</span></label>
                                            <select id="categorie_produit_id" name="categorie_produit_id" class="form-control color-gray @error('categorie_produit_id') border border-danger @enderror">
                                                <option disabled>Choisir la categorie</option>
                                                @foreach ($cat_produits as $categorieproduit)
                                                    <option value="{{$categorieproduit->id}}"
                                                        @if ($produit->categorie_produit_id == $categorieproduit->id)
                                                            selected
                                                        @endif>
                                                        {{$categorieproduit->nom_categorie_produit}}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('categorie_produit_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="boutique_id" class="text-dark font-weight-semibold font-size-md mb-2 lh-15">Boutique
                                                <span class="text-danger">*</span></label>
                                            <select id="boutique_id" name="boutique_id" class="form-control color-gray @error('boutique_id') border border-danger @enderror">
                                                <option disabled>Choisir la boutique</option>
                                                @foreach ($boutiques as $boutique)
                                                    <option value="{{ $boutique->id }}"
                                                        @if ($produit->boutique_id == $boutique->id)
                                                            selected
                                                        @endif>
                                                        {{$boutique->nom_boutique}}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('boutique_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="prix_produit" class="font-size-md text-dark font-weight-semibold mb-1">Prix
                                                <span class="text-danger">*</span></label>
                                            <input type="number" class="form-control @error('prix_produit') border border-danger @enderror" value="{{$produit->prix_produit}}" id="prix_produit" name="prix_produit" placeholder="Nom Categorie">
                                            @error('prix_produit')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="photo_produit" class="font-size-md text-dark font-weight-semibold mb-1">Photo
                                                <span class="text-danger">*</span></label>
                                            <input type="file" class="form-control @error('photo_produit') border border-danger @enderror" id="photo_produit" name="photo_produit">
                                            @error('photo_produit')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-2">
                                            <label for="description_produit" class="font-size-md text-dark font-weight-semibold mb-1">Description</label>
                                            <textarea type="text" class="form-control " value="{{$produit->description_produit}}" id="description_produit" name="description_produit" placeholder="Description"></textarea>

                                        </div>
                                        <div class="pt-8">
                                            <div class="contact-section text-center pt-3">
                                                <button type="submit"
                                                    class="btn btn-primary btn-block font-size-h5 lh-19 mt-2">Enregistrer</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="mt-auto pt-6"><a href="/" class="link-hover-dark-primary font-weight-semibold">Boutique Senegal</a>
                        Tout droit réservé. Développé par
                    <a href="https://inovtechsenegal.com" class="link-hover-dark-primary font-weight-semibold">Inovtech</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
