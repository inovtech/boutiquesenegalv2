@extends('layouts.guest')

@section('content')
    <header id="header"
        class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                            data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/boutique-du-jour">Boutique du Jour <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/categorie">Catégories <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                            </li>
                            @auth
                                @if (Auth::user()->role_id == 1)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @elseif(Auth::user()->role_id == 2)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                    connecter<span class="caret"></i></span></a>
                            </li>
                            @endauth
                            <li class="nav-item ">
                                <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                    Ajoutez votre boutique<span class="caret"></i></span></a>
                            </li>
                        </ul>
                        <!-- {{-- <div class="header-customize justify-content-end align-items-center d-none d-xl-flex ml-auto"> --}}
                                {{-- <div class="header-customize-item">
                                    <a class="nav-link" href="/categorie">
                                        Categorie
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/blog">
                                        Catégories
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/pricing">
                                        Offres
                                    </a>
                                </div> --}}
                                {{-- <div class="header-customize-item">
                                    <a href="{{ url('login') }}" class="link">
                                        <svg class="icon icon-user-circle-o">
                                            <use xlink:href="#icon-user-circle-o"></use>
                                        </svg>
                                        Se connecter
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a href="{{ route('client.create-shop') }}" class="btn btn-primary text-capitalize">
                                        + Ajoutez votre boutique
                                    </a>
                                </div>
                            </div> --}} -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <section class="banner">
            <div class="container">
                <div class="banner-content">
                    <div class="heading" data-animate="fadeInLeft">
                        <h1 class="mb-0 text-white">
                            <span class="d-block lh-1 bleu">Boutique Sénégal</span>
                            <span class="d-block lh-1">1er Annuaire de Boutiques en ligne</span>
                        </h1>
                    </div>
                    <div class="form-search" data-animate="fadeInRight">
                        <form action="{{route('guest.search')}}" method="GET">
                            <div class="row align-items-end">
                                <div class="col-xl-6 mb-4 mb-xl-0">
                                    <label for="key-word" class="text-white font-weight-bold text-uppercase">
                                        Que chercher vous ?</label>
                                    <div class="input-group rounded">
                                        <input type="text" id="key-word" name="search_query"
                                            class="form-control font-size-lg border-0 form-control-lg"
                                            placeholder="Ex: Chaussure, Robe, Bijou, Maquillage..."
                                            data-toggle="dropdown" aria-haspopup="true"
                                            autocomplete="off" />
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <button type="submit"
                                        class="btn btn-primary-search font-weight-bold font-size-h5 btn-block btn-icon-left btn-lg lh-16">
                                        <i class="fal fa-search"></i>Chercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    <div id="wrapper-content" class="wrapper-content pt-12">
            <div class="container">
                <div class="page-container row">
                    <div class="col-lg-3 mb-4 mb-lg-0">
                        <div class="sidebar shop-sidebar primary-sidebar sidebar-sticky" id="sidebar">
                            <div class="primary-sidebar-inner">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item px-0 pt-0 pb-8">
                                        <div class="card border-0 rounded-0 p-0">
                                            <div class="card-body product-category p-0">
                                                <h5 class="card-title mb-7 lh-1">ESPACE PUB</h5>
                                                <div class="campaign">
                                                    <img src="images/other/pub.jpg" alt="Campaign">
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="page-content">
                            <div class="product-items pt-4">
                                <div class="row">
                                    @if ($produits->count() == 0)
                                        <h1 style="font-size: 20px;">Aucun produit trouvé</h1>
                                    @endif
                                    @foreach ($produits as $produit)
                                        <div class="col-md-6 col-lg-4 mb-5">
                                            <div class="product card border-0 rounded-0 p-0">
                                                <div class="position-relative h-100">
                                                    <a href="{{ route('guest.boutique-index', [$produit->slug_nom_boutique]) }}">
                                                        <img src="{{$produit->photo_produit}}" alt="Product 4" style="width: 240px; height:230px;"
                                                            class="card-img-top">
                                                    </a>
                                                </div>
                                                <div class="card-body text-center position-relative">
                                                    <a href="{{ route('guest.boutique-index', [$produit->slug_nom_boutique]) }}"
                                                        class="link-hover-secondary-primary font-size-md mb-1">{{$produit->nom_produit}}</a>
                                                    <div class="product-meta-wrapper position-relative">
                                                        <div class="add-to-cart position-absolute w-100">
                                                            <div class="font-size-md">
                                                                <span class="text-dark">{{$produit->prix_produit}}</span> FCFA
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="add-to-cart position-absolute w-100">
                                                            <a href="{{ route('guest.boutique-index', [$produit->slug_nom_boutique]) }}"
                                                                class="link-hover-dark-primary font-weight-semibold text-uppercase">Voir Boutique</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <ul class="pagination pagination-style-01 justify-content-center mt-6">
                                {{$produits->links()}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
