<header id="header" class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{asset('images/white-logo.png')}}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#3
                        arch-popup" data-gtf-mfp="true"
                        data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex align-items-center" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{asset('images/white-logo.png')}}" alt="Boutique Senegal" />
                        </a>
                        <div class="header-customize justify-content-end align-items-center d-none d-xl-flex ml-auto">
                            <div class="header-customize-item">
                                <a class="nav-link" href="/categorie">
                                    Catégories
                                </a>
                            </div>
                            <div class="header-customize-item">
                                <a class="nav-link" href="/blog">
                                    Blog
                                </a>
                            </div>
                            <div class="header-customize-item">
                                <a class="nav-link" href="/pricing">
                                    Pricing
                                </a>
                            </div>
                            <div class="header-customize-item">
                                <a href="{{url('login')}}" class="link">
                                    <svg class="icon icon-user-circle-o ">
                                        <use xlink:href="#icon-user-circle-o"></use>
                                    </svg>
                                    Log in
                                </a>
                            </div>
                            <div class="header-customize-item">
                                <a href="{{route('client.create-shop')}}" class="btn btn-primary text-capitalize">
                                    + Ajoutez votre boutique
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>