@extends('layouts.guest')
@section('content')
<header id="header"
        class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                            data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="/categorie">Catégories <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                            </li>
                            @auth
                                @if (Auth::user()->role_id == 1)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @elseif(Auth::user()->role_id == 2)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                    connecter<span class="caret"></i></span></a>
                            </li>
                            @endauth
                            <li class="nav-item ">
                                <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                    Ajoutez votre boutique<span class="caret"></i></span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div id="page-title" class="page-title page-title-style-background">
        <div class="container">
            <div class="h-100 d-flex flex-column justify-content-center text-center">
                <h1 class="mb-0" data-animate="fadeInDown">
                    <span class="font-weight-light">Catégorie</span>
                    <span class="bleu">{{$categorie->nom_categorie_boutique}} </span>
                </h1>
                <ul class="breadcrumb breadcrumb-style-01 justify-content-center" data-animate="fadeInUp">
                    <li class="breadcrumb-item">
                        <a href="{{url('/')}}" class="link-hover-dark-primary">Accueil</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/categorie')}}">
                            <span>Catégories</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="wrapper-content" class="wrapper-content bg-gray-04 pb-0">
			<div class="container">
				<ul class="breadcrumb breadcrumb-style-02 py-7">
					<li class="breadcrumb-item"><a href="{{url('/')}}">Accueil</a></li>
					<li class="breadcrumb-item"><a href="{{url('/categorie')}}">Catégorie</a></li>
					<li class="breadcrumb-item">{{$categorie->nom_categorie_boutique}}</li>
				</ul>
				<div class="page-container row">
					<div class="page-content col-12 col-md-12 order-0 order-lg-1 mb-8 mb-lg-0">
						<div class="explore-filter d-lg-flex align-items-center d-block">
							<div class="text-dark font-weight-semibold font-size-md mb-4 mb-lg-0">{{$boutiques->count()}} Résultats trouvés</div>
						</div>
						<div class="row equal-height">
                            @foreach ($boutiques as $boutique)
                                <div class="col-lg-4 mb-4">
                                    <div class="store card border-0 rounded-0">
                                        <div class="position-relative store-image">
                                            <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}">
                                                <img src="{{$boutique->photo_boutique}}" alt="store 1"
                                                    class="card-img-top rounded-0 img-fluid" style="height:300px">
                                            </a>
                                            <div class="image-content position-absolute d-flex align-items-center">
                                                <div class="content-right ml-auto d-flex">
                                                    <a href="{{$boutique->photo_boutique}}" class="item viewing"
                                                        data-toggle="tooltip" data-placement="top" title="Quick view"
                                                        data-gtf-mfp="true">
                                                        <svg class="icon icon-expand">
                                                            <use xlink:href="#icon-expand"></use>
                                                        </svg>
                                                    </a>
                                                    <a href="#" class="item marking" data-toggle="tooltip"
                                                        data-placement="top" title="Bookmark"><i
                                                            class="fal fa-bookmark"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body pb-2">
                                            <a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}"
                                                class="card-title h5 text-dark d-inline-block mb-2"><span
                                                    class="letter-spacing-25 text-uppercase">{{$boutique->nom_boutique}}</span></a>
                                            <ul
                                                class="list-inline store-meta mb-2 font-size-sm d-flex align-items-center flex-wrap">
                                                <li class="list-inline-item"><span class="mr-1">Catégorie : </span><span
                                                        class=" font-weight-semibold" style="color:#39d1ff">{{$boutique->nom_categorie_boutique}}</span></li>
                                                <li class="list-inline-item separate"></li>
                                                <li class="list-inline-item"><span class="text-green">Ouverte</span></li>
                                            </ul>
                                        </div>
                                        <div class="card-footer rounded-0 border-top-0 pb-3 pt-0 bg-transparent">
                                            <div class="border-top pt-3">
                                                <span class="d-inline-block mr-1"><i class="fal fa-map-marker-alt">
                                                    </i>
                                                </span>
                                                <a href="#" class="text-secondary text-decoration-none address">
                                                    {{$boutique->adresse_boutique}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
						</div>
						<div class="pagination mt-6 mb-6">
							{{$boutiques->links()}}
						</div>
					</div>
				</div>
			</div>
			<div class="recent-view pt-9 bg-white pb-10">
				<div class="container">
					<div class="mb-6">
						<h5 class="mb-0">
							Boutiques récentess
						</h5>
					</div>
					<div class="row">
					@foreach ($boutique_4 as $boutique)
						<div class="col-lg-4 mb-4 mb-lg-0">
							<div class="store media align-items-stretch bg-white">
								<div class="store-image position-relative">
									<a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}">
										<img src="{{$boutique->photo_boutique}}" alt="Recent view 1">
									</a>
									<div class="image-content position-absolute d-flex align-items-center">
										<div class="content-right ml-auto d-flex">
											<a href="{{$boutique->photo_boutique}}" class="item viewing"
												data-toggle="tooltip" data-placement="top" title="Quickview"
												data-gtf-mfp="true">
												<svg class="icon icon-expand">
													<use xlink:href="#icon-expand"></use>
												</svg>
											</a>
										</div>
									</div>
								</div>
								<div class="media-body pl-0 pl-sm-3 pt-4 pt-sm-0">
									<a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}"
										class="font-size-md font-weight-semibold text-dark d-inline-block mb-2 lh-11"><span
											class="letter-spacing-25">{{$boutique->nom_boutique}}</span> </a>
									<div>
										<span class="d-inline-block mr-1"><i class="fal fa-map-marker-alt">
											</i>
										</span>
										<a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" class="text-secondary text-decoration-none address">{{$boutique->adresse_boutique}}</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
					</div>
				</div>
			</div>
		</div>
@endsection
