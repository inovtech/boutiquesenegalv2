@extends('layouts.app')

@section('content')
<section class="tz-register">
        <div class="log-in-pop">
            <div class="log-in-pop-left">
                <h1>Boujour.... <span></span></h1>
                <p>Vous n'avez pas de compte? Créez votre compte. Ça prend moins d'une minute</p>
                <h4>Connectez-vous avec les médias sociaux</h4>
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i> Facebook</a>
                    </li>
                    <li><a href="#"><i class="fa fa-google"></i> Google+</a>
                    </li>
                    <li><a href="#"><i class="fa fa-twitter"></i> Twitter</a>
                    </li>
                </ul>
            </div>
            <div class="log-in-pop-right">
                <h4>Créer un compte</h4>
                <p>Vous n'avez pas de compte? Veuillez créer votre compte, ça vous prendrez moins d'une minute</p>
                <form method="POST" action="{{ route('register') }}" class="s12">
                    @csrf
                    <div>
                        <div class="input-field s12">
                            <input type="text" data-ng-model="prenom_user" name="prenom_user" class="validate" value="old('prenom_user')" required autofocus autocomplete="prenom_user">
                            <label>Prenom</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12">
                            <input type="text" data-ng-model="nom_user" name="nom_user" class="validate" value="old('nom_user')" required autocomplete="nom_user">
                            <label>Nom</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12">
                            <input type="email" data-ng-model="email" name="email" class="validate" value="old('email')" required >
                            <label>Adresse Email</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12">
                            <input type="tel" data-ng-model="telephone_user" name="telephone_user" class="validate" value="old('telephone_user')" required autocomplete="telephone_user">
                            <label>Numero Telephone</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12">
                            <input type="text" data-ng-model="adresse_user" name="adresse_user" class="validate" value="old('adresse_user')" required >
                            <label>Adresse Domicile</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12">
                            <input type="password" data-ng-model="password" name="password" class="validate" value="old('password')" required autocomplete="new-password" >
                            <label>Password</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12">
                            <input type="password" data-ng-model="password_confirmation" name="password_confirmation" class="validate" value="old('password_confirmation')" required autocomplete="new-password" >
                            <label>Confirm password</label>
                        </div>
                    </div>
                    <div>
                        <div class="input-field s4">
                            <input type="submit" value="Register" class="waves-effect waves-light log-in-btn">
                        </div>
                    </div>
                    <div>
                        <div class="input-field s12"> <a href="{{ route('login') }}">Avez-vous déjà un compte?</a> </div>
                    </div>
                </form>
            </div>
        </div>
</section>
@endsection
