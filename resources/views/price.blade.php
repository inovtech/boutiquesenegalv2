@extends('layouts.guest')
@section('content')
<header id="header"
        class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                            data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/categorie">Catégories <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="/pricing">Offres<span class="caret"></i></span></a>
                            </li>
                            @auth
                                @if (Auth::user()->role_id == 1)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @elseif(Auth::user()->role_id == 2)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                    connecter<span class="caret"></i></span></a>
                            </li>
                            @endauth
                            <li class="nav-item ">
                                <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                    Ajoutez votre boutique<span class="caret"></i></span></a>
                            </li>
                        </ul>
                        <!-- {{-- <div class="header-customize justify-content-end align-items-center d-none d-xl-flex ml-auto"> --}}
                                {{-- <div class="header-customize-item">
                                    <a class="nav-link" href="/categorie">
                                        Categorie
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/blog">
                                        Catégories
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/pricing">
                                        Offres
                                    </a>
                                </div> --}}
                                {{-- <div class="header-customize-item">
                                    <a href="{{ url('login') }}" class="link">
                                        <svg class="icon icon-user-circle-o">
                                            <use xlink:href="#icon-user-circle-o"></use>
                                        </svg>
                                        Se connecter
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a href="{{ route('client.create-shop') }}" class="btn btn-primary text-capitalize">
                                        + Ajoutez votre boutique
                                    </a>
                                </div>
                            </div> --}} -->
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <div id="page-title" class="page-title text-center page-title-style-background">
        <div class="container">
            <div class="h-100 d-flex flex-column justify-content-center">
                <div class="heading">
                    <h1 class="mb-0" data-animate="fadeInDown">
                        <span class="font-weight-light">Nos </span>
                        <span class="bleu">Offres</span>
                    </h1>
                    <ul class="breadcrumb justify-content-center" data-animate="fadeInUp">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item"><span>Offres</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div id="wrapper-content" class="wrapper-content pt-10 pb-13">
        <div class="container">
            <div class="page-description text-center">
                Nous vous proposons des offres adaptées à vos besoins !
            </div>

            <div id="pricing-table" class="section-pricing-table pt-9">
                <div class="card-deck">
                    <div class="pricing-table card rounded-0">
                        <div class="card-header bg-transparent border-0 p-0">
                            <div class="font-weight-semibold text-dark font-size-md mb-3 text-uppercase">
                            DECOUVERTE
                            </div>
                            <div class="mb-5">
                                <span class="price text-primary">10 000 FCFA </span>
                                <span>/</span>
                                <span>An</span>
                            </div>
                            <p class="text-gray mb-6">Pour vous faire découvrir ce qu'on peut vous apporter.</p>
                        </div>
                        <div class="card-body px-0 pt-5 pb-7">
                            <ul class="features list-group list-group-flush list-group-borderless">
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Durée 1 an</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">1 Boutique</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">3 Photos</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Vos Coordonnées</span>
                                </li>
                            </ul>
                        </div>
                        <div class="card-footer bg-transparent border-0 mt-auto p-0">
                            <a href="{{ route('client.create-shop') }}" class="btn btn-primary btn-block lh-lg font-weight-bold rounded-0">Choisir</a>
                        </div>
                    </div>
                    <div class="pricing-table card rounded-0 bg-primary text-white">
                        <div class="card-header bg-transparent border-0 p-0">
                            <div class="font-weight-semibold font-size-md mb-3 text-uppercase">ESSENTIEL</div>
                            <div class="mb-5">
                                <span class="price text-white">50 000 FCFA </span>
                                <span>/</span>
                                <span>An</span>
                            </div>
                            <p class="mb-6">Pour vous montrer un aperçu de nos différents outils.</p>
                        </div>
                        <div class="card-body px-0 pt-5 pb-7">
                            <ul class="features list-group list-group-flush list-group-borderless">
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span>Durée 1 an</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span> 1 Boutique</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span>5 photos</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span>Vos Coordonnées</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span>3 Produits</span>
                                </li>
                            </ul>
                        </div>
                        <div class="card-footer bg-transparent border-0 p-0 mt-auto">
                            <a href="{{ route('client.create-shop') }}" class="btn btn-white lh-lg text-primary btn-block font-weight-bold rounded-0">Choisir</a>
                        </div>
                    </div>
                    <div class="pricing-table card rounded-0">
                        <div class="card-header bg-transparent border-0 p-0">
                            <div class="font-weight-semibold text-dark font-size-md mb-3 text-uppercase">
                            Premium
                            </div>
                            <div class="mb-5">
                                <span class="price text-primary">100 000 FCFA</span>
                                <span>/</span>
                                <span>An</span>
                            </div>
                            <p class="text-gray mb-6">Vous aider à mieux développer votre business.</p>
                        </div>
                        <div class="card-body px-0 pt-5 pb-7">
                            <ul class="list-group list-group-flush list-group-borderless">
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Durée 1 an</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark"> 2 Boutiques</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">5 photos</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Vos coordonnées</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">6 Produits</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">1 Design pour une affiche</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Publicité sur nos réseaux sociaux</span>
                                </li>
                            </ul>
                        </div>
                        <div class="card-footer bg-transparent border-0 p-0 mt-auto">
                            <a href="{{ route('client.create-shop') }}" class="btn btn-primary btn-block lh-lg font-weight-bold rounded-0">Choisir</a>
                        </div>
                    </div>
                    <!-- <div class="pricing-table card rounded-0">
                        <div class="card-header bg-transparent border-0 p-0">
                            <div class="font-weight-semibold text-dark font-size-md mb-3 text-uppercase">Premium</div>
                            <div class="mb-5">
                                <span class="price text-primary">10 000 FCFA</span>
                                <span>/</span>
                                <span>An</span>
                            </div>
                            <div class="text-gray mb-6">Sed faucibus lectus quis tellus fermentum.</div>
                        </div>
                        <div class="card-body px-0 pt-5 pb-7">
                            <ul class="list-group list-group-flush list-group-borderless">
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Duration: 30 days</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark"> 30 Listing</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Contact Display</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Price Range</span>
                                </li>
                                <li class="list-group-item bg-transparent p-0 mb-1">
                                    <span class="text-green font-size-md d-inline-block mr-3"><i class="fal fa-check"></i></span>
                                    <span class="text-dark">Business Hours</span>
                                </li>
                            </ul>
                        </div>
                        <div class="card-footer bg-transparent border-0 p-0 mt-auto">
                            <a href="#" class="btn btn-primary btn-block lh-lg font-weight-bold rounded-0">Choisir</a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
@endsection
