@extends('layouts.guest')
@section('content')
<header id="header"
    class="main-header header-float header-sticky header-sticky-smart header-light header-style-02 font-normal">
    <div class="header-wrapper sticky-area">
        <div class="container">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="/">
                        <img src="{{ asset('images/logo1.png') }}" alt="Boutique Senegal" />
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                        data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                        <i class="far fa-search"></i>
                    </a>
                </div>
                <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="/">
                        <img src="{{ asset('images/logo1.png') }}" alt="Boutique Senegal" />
                    </a>
                    <ul class="navbar-nav ml-auto bleuhov">
                        <li class="nav-item">
                            <a class="nav-link" href="/categorie">Catégories <span class="caret"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                        </li>
                        @auth
                            @if (Auth::user()->role_id == 1)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                </li>

                            @elseif(Auth::user()->role_id == 2)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                </li>

                            @endif
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                connecter<span class="caret"></i></span></a>
                        </li>
                        @endauth
                        <li class="nav-item ">
                            <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                Ajoutez votre boutique<span class="caret"></i></span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
<br><br><br>

<div id="page-title" class="page-title py-6">
    <div class="container">
        <div class="h-100 ">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="/" class="text-decoration-none">Accueil</a></li>
                <li class="breadcrumb-item"><a href="{{ route('guest.boutique-index', [$boutique->slug_nom_boutique]) }}" class="text-decoration-none">{{$boutique->nom_boutique}} </a></li>
                <li class="breadcrumb-item"><span>{{$produit->nom_produit}}</span></li>
            </ul>
        </div>
    </div>
</div>


<div id="wrapper-content" class="wrapper-content pt-9 pb-12">
    <div class="container">
        <div class="page-container">
            <div class="product single-product row">
                <div class="product-images col-lg-8 d-flex mb-5 mb-lg-0">
                    <div class="slick-slider slider-nav slider-vertical"
                        data-slick-options='{"slidesToShow": 4,"autoplay":true,"dots":false,"arrows":false,"vertical":true,"verticalSwiping":true,"asNavFor": ".slider-for","focusOnSelect": true,"responsive":[{"breakpoint": 768,"settings": {"vertical":false,"verticalSwiping":true}}]}'>
                        <div class="box"><img src="{{$produit->photo_produit}}" style="width:70px" alt="Gallery 01"></div>
                    </div>
                    <div class="slick-slider slider-for slider-vertical"
                        data-slick-options='{"slidesToShow": 1,"autoplay":false,"dots":false,"arrows":false,"asNavFor": ".slider-nav","vertical":true,"verticalSwiping":true,"responsive":[{"breakpoint": 768,"settings": {"vertical":false,"verticalSwiping":false}}]}'>
                        <div class="box">
                            <div class="image">
                                <img src="{{$produit->photo_produit}}" alt="Gallery 01">
                                <div class="image-icon">+</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-details col-lg-4">
                    <div class="font-size-h5 text-dark lh-1625">{{$produit->nom_produit}}</div>
                    <div class="author-rate d-flex mb-3">
                        <span class="rate-item checked">
                            <svg class="icon icon-ionicons_svg_md-star">
                                <use xlink:href="#icon-ionicons_svg_md-star"></use>
                            </svg>
                        </span>
                        <span class="rate-item checked">
                            <svg class="icon icon-ionicons_svg_md-star">
                                <use xlink:href="#icon-ionicons_svg_md-star"></use>
                            </svg>
                        </span>
                        <span class="rate-item checked">
                            <svg class="icon icon-ionicons_svg_md-star">
                                <use xlink:href="#icon-ionicons_svg_md-star"></use>
                            </svg>
                        </span>
                        <span class="rate-item checked">
                            <svg class="icon icon-ionicons_svg_md-star">
                                <use xlink:href="#icon-ionicons_svg_md-star"></use>
                            </svg>
                        </span>
                        <span class="rate-item checked">
                            <svg class="icon icon-ionicons_svg_md-star">
                                <use xlink:href="#icon-ionicons_svg_md-star"></use>
                            </svg>
                        </span>
                        <span class="d-inline-block ml-2 text-gray">3 customer reviews</span>
                    </div>
                    <div class="mb-6">
                        <span class="text-danger font-size-h5">{{$produit->prix_produit}} FCFA</span>
                        {{-- <span class="font-size-md text-dark text-decoration-line-through">$34.50</span> --}}
                    </div>
                    <div class="text-gray mb-8 pb-1">
                        {{$produit->description_produit}}
                    </div>
                    <form>
                        <div class="form-add-cart d-flex flex-wrap flex-sm-nowrap mb-6">
                            <div class="input-group align-items-center flex-nowrap col-sm-4 px-3">
                                <a href="#" class="input-group-prepend"><i class="fal fa-minus"></i></a>
                                <input type="text" class="form-control bg-transparent border-0 text-center"
                                    value="02">
                                <a href="#" class="input-group-append text-decoration-none"><i
                                        class="fal fa-plus"></i></a>
                            </div>
                            <div class="button-add-cart pl-0 pl-sm-5">
                                <button class="btn btn-primary text-uppercase btn-block font-size-md rounded-0">
                                    Add To Cart
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="border-top mb-6 pt-6">
                        <div class="d-flex">
                            <span class="text-gray d-inline-block mr-1">SKU: </span>
                            <span class="text-dark">{{$produit->slug_nom_produit}}</span>
                        </div>
                        <div class="d-flex">
                            <span class="text-gray d-inline-block mr-1">Categories: </span>
                            <a href="#" class="link-hover-dark-primary d-inline-block mr-1">{{$produit->categorie->nom_categorie_produit}}</a>
                        </div>
                        {{-- <div class="d-flex">
                            <span class="text-gray d-inline-block mr-1">Tags: </span>
                            <a href="#" class="link-hover-dark-primary d-inline-block mr-1">tshirt, </a>
                            <a href="#" class="link-hover-dark-primary d-inline-block mr-1">pattern, </a>
                            <a href="#" class="link-hover-dark-primary">man</a>
                        </div> --}}
                    </div>
                    <div class="social-icon light-color">
                        <ul class="list-inline text-left">
                            @if ($boutique->link_twitter_boutique != null)
                                <li class="list-inline-item">
                                    <a target="_blank" title="Twitter" href="{{$boutique->link_twitter_boutique}}">
                                        <i class="fab fa-twitter">
                                        </i>
                                        <span>Twitter</span>
                                    </a>
                                </li>
                            @endif
                            @if ($boutique->link_twitter_boutique != null)
                                <li class="list-inline-item">
                                    <a target="_blank" title="Facebook" href="{{$boutique->link_facebook_boutique}}">
                                        <i class="fab fa-facebook-f">
                                        </i>
                                        <span>Facebook</span>
                                    </a>
                                </li>
                            @endif
                            @if ($boutique->link_twitter_boutique != null)
                                <li class="list-inline-item">
                                    <a target="_blank" title="Instagram" href="{{$boutique->link_instagram_boutique}}">
                                        <svg class="icon icon-instagram">
                                            <use xlink:href="#icon-instagram"></use>
                                        </svg>
                                        <span>Instagram</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="mt-13 mb-12">
                <div class="collapse-tabs">
                    <div class="tabs pb-3 mb-8 border-bottom d-none d-sm-block">
                        <ul class="nav nav-pills tab-style-01 text-capitalize fs-20 justify-content-center"
                            role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="description-tab" data-toggle="tab"
                                    href="#description" role="tab" aria-controls="description"
                                    aria-selected="true">Description</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content pt-2">
                        <div id="collapse-tabs-accordion">
                            <div class="tab-pane fade show active px-0 px-md-12 mb-4 mb-sm-0" id="description"
                                role="tabpanel" aria-labelledby="description-tab">
                                <div class="card bg-transparent mb-4 mb-sm-0">
                                    <div class="card-header d-block d-sm-none bg-transparent px-0 py-1"
                                        id="headingDescription">
                                        <h5 class="mb-0">
                                            <button class="btn font-size-h5 btn-block" data-toggle="collapse"
                                                data-target="#description-collapse" aria-expanded="true"
                                                aria-controls="description-collapse">
                                                Description
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="description-collapse" class="collapse show collapsible"
                                        aria-labelledby="headingDescription"
                                        data-parent="#collapse-tabs-accordion">
                                        <div class="card-body p-sm-0 border-sm-0">
                                            <div class="lh-lg text-center">
                                                <p class="mb-6">
                                                    {{$produit->description_produit}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="relate-products">
                <h5 class="mb-9 text-center">Produits Similaires</h5>
                <div class="slick-slider"
                    data-slick-options='{"slidesToShow": 4,"autoplay":true,"dots":false,"arrows":true,"responsive":[{"breakpoint": 1000,"settings": {"slidesToShow": 2}},{"breakpoint": 770,"settings": {"slidesToShow": 1,"arrows":false}}]}'>
                    @foreach ($produit_smlaires as $item)
                        <div class="box">
                            <div class="product card border-0 rounded-0 p-0">
                                <div class="position-relative h-100">
                                    <a href="{{ route('guest.produit-index', [$item->slug_nom_boutique, $item->slug_nom_produit]) }}">
                                        <img src="{{$item->photo_produit}}" style="width: 250px; height: 250px" alt="Product 1" class="card-img-top">
                                    </a>
                                </div>
                                <div class="card-body text-center position-relative">
                                    <a href="{{ route('guest.produit-index', [$item->slug_nom_boutique, $item->slug_nom_produit]) }}"
                                        class="link-hover-secondary-primary font-size-md mb-1">
                                        {{$item->nom_produit}}
                                    </a><br>
                                    <div class="product-meta-wrapper position-relative">
                                        <div class="product-meta position-absolute">
                                            <div class="font-size-md">
                                                <span class="text-danger">{{$item->prix_produit}} FCFA</span>
                                            </div>
                                        </div><br>
                                        <div class="add-to-cart position-absolute w-100">
                                            <a href="{{ route('guest.produit-index', [$item->slug_nom_boutique, $item->slug_nom_produit]) }}"
                                                class="link-hover-dark-primary font-weight-semibold text-uppercase">
                                                Voir Produit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
    <style>
        .navbar {
            overflow: hidden;
            background-color: #333;
            position: fixed !important; /* Set the navbar to fixed position */
            top: 0; /* Position the navbar at the top of the page */
            width: 100%; /* Full width */
        }
    </style>
@endsection
