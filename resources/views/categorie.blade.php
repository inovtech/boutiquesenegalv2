@extends('layouts.guest')
@section('content')
<header id="header"
        class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                            data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/boutique-du-jour">Boutique du Jour <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="/categorie">Catégories <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                            </li>
                            @auth
                                @if (Auth::user()->role_id == 1)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @elseif(Auth::user()->role_id == 2)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>

                                @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                    connecter<span class="caret"></i></span></a>
                            </li>
                            @endauth
                            <li class="nav-item ">
                                <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                    Ajoutez votre boutique<span class="caret"></i></span></a>
                            </li>
                        </ul>
                        <!-- {{-- <div class="header-customize justify-content-end align-items-center d-none d-xl-flex ml-auto"> --}}
                                {{-- <div class="header-customize-item">
                                    <a class="nav-link" href="/categorie">
                                        Categorie
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/blog">
                                        Catégories
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a class="nav-link" href="/pricing">
                                        Offres
                                    </a>
                                </div> --}}
                                {{-- <div class="header-customize-item">
                                    <a href="{{ url('login') }}" class="link">
                                        <svg class="icon icon-user-circle-o">
                                            <use xlink:href="#icon-user-circle-o"></use>
                                        </svg>
                                        Se connecter
                                    </a>
                                </div>
                                <div class="header-customize-item">
                                    <a href="{{ route('client.create-shop') }}" class="btn btn-primary text-capitalize">
                                        + Ajoutez votre boutique
                                    </a>
                                </div>
                            </div> --}} -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div id="page-title" class="page-title page-title-style-background">
        <div class="container">
            <div class="h-100 d-flex flex-column justify-content-center text-center">
                <h1 class="mb-0" data-animate="fadeInDown">
                    <span class="font-weight-light">Nos </span>
                    <span class="bleu">Catégories</span>
                </h1>
                <ul class="breadcrumb breadcrumb-style-01 justify-content-center" data-animate="fadeInUp">
                    <li class="breadcrumb-item">
                        <a href="{{url('/')}}" class="link-hover-dark-primary">Accueil</a>
                    </li>
                    <li class="breadcrumb-item"><span>Catégorie</span></li>
                </ul>
            </div>
        </div>
    </div>
    <section id="section-05" class="pt-12 pb-13 bg-pattern-01">
        <div class="container">
            <div class="text-center mb-8">
                <h3 class="mb-0 bleu">Nos Catégories</h3>
            </div>
            <div class="row">
                @foreach ($categories as $categorie)
                    <div class="col-md-4 mb-6">
                        <div class="image-box card mb-6 rounded-0 border-0 hover-scale" data-animate="zoomIn">
                            @if ($categorie->photo_categorie_boutique == null)
                            <a href="{{ route('guest.categorie-index', [$categorie->slug_categorie_boutique]) }}" class="image position-relative card-img">
                                <img src="{{asset('images/listing/categorie.jpg')}}" alt="Catégorie Image" style="width: 360px;  height:250px;" />
                            </a>
                            @else
                            <a href="{{ route('guest.categorie-index', [$categorie->slug_categorie_boutique]) }}" class="image position-relative card-img">
                                <img src="{{$categorie->photo_categorie_boutique}}" alt="Catégorie Image" style="width: 360px; height:250px;"/>
                            </a>
                            @endif
                            <div class="text-white content-box px-4 pb-3 card-img-overlay">
                                @if ($categorie->boutiques_count == 0)
                                    <p class="mb-1 bleu">{{$categorie->boutiques_count}} Boutique</p>
                                @else
                                    <p class="mb-1 bleu">{{$categorie->boutiques_count}} Boutiques</p>
                                @endif
                                <a href="{{ route('guest.categorie-index', [$categorie->slug_categorie_boutique]) }}" class="font-weight-normal text-white font-size-lg">
                                    {{$categorie->nom_categorie_boutique}}
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
