@extends('layouts.guest')
@section('content')
<header id="header"
    class="main-header header-float header-sticky header-sticky-smart header-light header-style-02 font-normal">
    <div class="header-wrapper sticky-area">
        <div class="container">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="/">
                        <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                        data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                        <i class="far fa-search"></i>
                    </a>
                </div>
                <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="/">
                        <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                    </a>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link active" href="/boutique-du-jour">Boutique du Jour <span class="caret"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/categorie">Catégories <span class="caret"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                        </li>
                        @auth
                            @if (Auth::user()->role_id == 1)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                </li>
                            @elseif(Auth::user()->role_id == 2)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                </li>
                            @endif
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                connecter<span class="caret"></i></span></a>
                        </li>
                        @endauth
                        <li class="nav-item ">
                            <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                Ajoutez votre boutique<span class="caret"></i></span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pb-0 pt-11 bg-gray-06">
    <div class="mb-6">
        <div class="container align-content-center">
            <img src="{{$boutique->photo_boutique}}" style="width: 1000px; height: 470px;" alt="Full image">
        </div>
    </div>
    <div class="container">
        <div class="page-container row pt-1">
            <div class="page-content col-12 col-xl-8 mb-8 mb-xl-0">
                <ul class="breadcrumb breadcrumb-style-03 mb-6">
                    <li class="breadcrumb-item"><a href="{{route('index')}}">Acceuil</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('guest.categorie-index', [$boutique->categorie->slug_categorie_boutique]) }}">{{$boutique->categorie->nom_categorie_boutique}} </a></li>
                    <li class="breadcrumb-item">{{$boutique->nom_boutique}}</li>
                </ul>
                <div class="explore-details-top mb-8">
                    <div class="store mb-6">
                        <div class="d-flex flex-wrap">
                            <h2 class="store-name mr-2">{{$boutique->nom_boutique}}
                            </h2>
                            <span class="check font-weight-semibold mb-2 text-green">
                                <i class="fas fa-check-circle"></i>
                                Vérifié
                            </span>
                        </div>
                        <ul class="list-inline store-meta d-flex align-items-center flex-wrap">
                            <li class="list-inline-item"><span
                                    class="badge badge-success d-inline-block mr-1">5.0</span>
                                <span>4 notes</span>
                            </li>
                            <li class="list-inline-item separate"></li>
                            <li class="list-inline-item">
                                <a href="{{ route('guest.categorie-index', [$boutique->categorie->slug_categorie_boutique]) }}" class="text-link text-decoration-none d-flex align-items-center">
                                    <span>{{$boutique->categorie->nom_categorie_boutique}}</span>
                                </a>
                            </li>
                            <li class="list-inline-item separate"></li>
                            <li class="list-inline-item"><i class="fal fa-clock"></i>
                                Posted 15 hours ago
                            </li>
                        </ul>
                    </div>
                    <div class="d-flex flex-wrap flex-sm-nowrap">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="#"
                                    class="btn btn-white font-size-md mb-3 mb-sm-0 py-1 px-3 rounded-sm">
                                    <span class="d-inline-block mr-1">
                                        <i class="fal fa-share-alt"></i>
                                    </span>
                                    Partager</a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"
                                    class="btn btn-white font-size-md mb-3 mb-sm-0 py-1 px-2 rounded-sm">
                                    <span class="d-inline-block">
                                        <svg class="icon icon-chart-bars">
                                            <use xlink:href="#icon-chart-bars"></use>
                                        </svg>
                                    </span>
                                    Comparer</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="collapse-tabs">
                    <div class="tabs border-bottom pb-2 mb-6 d-none d-sm-block">
                        <ul class="nav nav-pills tab-style-01" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="description-tab" data-toggle="tab"
                                    href="#description" role="tab" aria-controls="description"
                                    aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews"
                                    role="tab" aria-controls="reviews" aria-selected="false">Gallerie
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div id="collapse-tabs-accordion">
                            <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                                <div class="card bg-transparent mb-4 mb-sm-0">
                                    <div class="card-header d-block d-sm-none bg-transparent px-0 py-1"
                                        id="headingDescription">
                                        <h5 class="mb-0">
                                            <button class="btn text-uppercase btn-block" data-toggle="collapse"
                                                data-target="#description-collapse" aria-expanded="true"
                                                aria-controls="description-collapse">
                                                Description
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="description-collapse" class="collapse show collapsible"
                                        aria-labelledby="headingDescription"
                                        data-parent="#collapse-tabs-accordion">
                                        <div class="card-body p-sm-0 border-sm-0">
                                            <h6 class="font-size-md mb-4">Introduce</h6>
                                            <div class="mb-7">
                                                <p class="mb-6">
                                                    {{$boutique->description_boutique}}
                                                </p>
                                            </div>
                                            <h6 class="font-size-md mb-4">
                                                Catégories Produits vendues
                                            </h6>
                                            <div class="row mb-7">
                                                <div class="col-md-4 mb-4 mb-md-0">
                                                    <ul
                                                        class="list-group list-group-flush list-group-borderless">
                                                        <li
                                                            class="list-group-item bg-transparent px-0 py-1 lh-15">
                                                            <span class="text-green d-inline-block mr-2"><i
                                                                    class="fas fa-check-circle"></i></span>
                                                            <span class="text-dark">Find & Pick Wood</span>
                                                        </li>
                                                        <li
                                                            class="list-group-item bg-transparent px-0 py-1 lh-15">
                                                            <span class="text-green d-inline-block mr-2"><i
                                                                    class="fas fa-check-circle"></i></span>
                                                            <span class="text-dark">Design Product</span>
                                                        </li>
                                                        <li
                                                            class="list-group-item bg-transparent px-0 py-1 lh-15">
                                                            <span class="text-green d-inline-block mr-2"><i
                                                                    class="fas fa-check-circle"></i></span>
                                                            <span class="text-dark">Product
                                                                implementation</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 mb-4 mb-md-0">
                                                    <ul
                                                        class="list-group list-group-flush list-group-borderless">
                                                        <li
                                                            class="list-group-item bg-transparent px-0 py-1 lh-15">
                                                            <span class="text-green d-inline-block mr-2"><i
                                                                    class="fas fa-check-circle"></i></span>
                                                            <span class="text-dark">Product Assembly</span>
                                                        </li>
                                                        <li
                                                            class="list-group-item bg-transparent px-0 py-1 lh-15">
                                                            <span class="text-green d-inline-block mr-2"><i
                                                                    class="fas fa-check-circle"></i></span>
                                                            <span class="text-dark">Shipping</span>
                                                        </li>
                                                        <li
                                                            class="list-group-item bg-transparent px-0 py-1 lh-15">
                                                            <span class="text-green d-inline-block mr-2"><i
                                                                    class="fas fa-check-circle"></i></span>
                                                            <span class="text-dark">Guarantee 1 Year</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                                <div class="card bg-transparent mb-4 mb-sm-0">
                                    <div class="card-header d-block d-sm-none bg-transparent px-0 py-1"
                                        id="headingReview">
                                        <h5 class="mb-0">
                                            <button class="btn text-uppercase btn-block collapsed"
                                                data-toggle="collapse" data-target="#reviews-collapse"
                                                aria-expanded="true" aria-controls="description-collapse">
                                                Gallerie
                                            </button>
                                        </h5>
                                    </div>
                                    @if ($galleries->count() > 0)
                                        <div class="card p-4 widget border-0 gallery bg-gray-06 rounded-0 mb-6">
                                            <div class="card-body px-0 pt-6 pb-3">
                                                <div class="photo-gallery d-flex flex-wrap justify-content-between">
                                                    @foreach ($galleries as $gallerie)
                                                        <a href="{{$gallerie->photo_gallerie}}" class="photo-item"
                                                            data-gtf-mfp="true" data-gallery-id="01">
                                                            <img src="{{$gallerie->photo_gallerie}}" style="width: 250px; height: 200px;" alt="Gallerie {{$gallerie->id}}"></a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    {{-- <div id="reviews-collapse" class="collapse collapsible"
                                        aria-labelledby="headingReview" data-parent="#collapse-tabs-accordion">
                                        <div class="card-body px-0 pt-6 pb-3">
                                                <div class="photo-gallery d-flex flex-wrap justify-content-between">
                                                    @foreach ($galleries as $gallerie)
                                                    <a href="{{$gallerie->photo_gallerie}}" class="photo-item"
                                                        data-gtf-mfp="true" data-gallery-id="01">
                                                        <img src="{{$gallerie->photo_gallerie}}" alt="Gallerie {{$gallerie->id}}"></a>
                                                    @endforeach
                                                </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-more-listing mt-8 border-top pt-6">
                    <div class="font-size-md font-weight-semibold mb-5 text-dark">Plus de produits de  <span
                            class="text-danger">{{$boutique->nom_boutique}}</span></div>
                    <div class="slick-slider arrow-top store-grid-style"
                        data-slick-options='{"slidesToShow": 2, "autoplay":false,"dots":false,"responsive":[{"breakpoint": 992,"settings": {"slidesToShow": 1}}]}'>
                        @foreach ($produits as $produit)
                            <div class="box">
                                <div class="store card rounded-0 border-0">
                                    <div class="position-relative store-image">
                                        <a href="{{ route('guest.produit-index', [$boutique->slug_nom_boutique, $produit->slug_nom_produit]) }}">
                                            <img src="{{$produit->photo_produit}}" style="width: 400px; height: 250px;" alt="store 1"
                                                class="card-img-top rounded-0">
                                        </a>
                                        <div class="image-content position-absolute d-flex align-items-center">
                                            <div class="content-right ml-auto d-flex">
                                                <a href="{{$produit->photo_produit}}" class="item viewing"
                                                    data-toggle="tooltip" data-placement="top" title="Quick view"
                                                    data-gtf-mfp="true">
                                                    <svg class="icon icon-expand">
                                                        <use xlink:href="#icon-expand"></use>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body pb-4 border-right border-left">
                                        <a href="{{ route('guest.produit-index', [$boutique->slug_nom_boutique, $produit->slug_nom_produit]) }}"
                                            class="card-title h5 text-dark d-inline-block mb-2"><span
                                                class="letter-spacing-25">{{$produit->nom_produit}}</span></a>
                                        <ul
                                            class="list-inline store-meta mb-4 font-size-sm d-flex align-items-center flex-wrap">
                                            <li class="list-inline-item">
                                                <span class="badge badge-success mr-1 d-inline-block">PRIX</span>
                                                <span class="number">{{$produit->prix_produit}} FCFA</span>
                                            </li>
                                            <li class="list-inline-item separate"></li>
                                            <li class="list-inline-item">
                                                <span class="badge badge-primary mr-1 d-inline-block">VOIR</span>
                                                <a class="text-uppercase" href="{{ route('guest.produit-index', [$boutique->slug_nom_boutique, $produit->slug_nom_produit]) }}">Contacter</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="sidebar col-12 col-xl-4">
                <div class="widget map mb-6 position-relative mb-6 rounded-0">
                    <div id="googleMap" data-google-map="true" class="small-map"
                        style="width:100%;height:240px;"></div>
                    <div class="button-direction position-absolute">
                        <a href="#" class="btn btn-sm btn-icon-left">
                            <i class="fas fa-location-arrow"></i>
                            Get Direction
                        </a>
                    </div>
                    <div class="card p-4 widget border-0 infomation pt-0 bg-white">
                        <div class="card-body px-0 py-2">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                    <span class="item-icon mr-3"><i class="fal fa-map-marker-alt"></i></span>
                                    <span class="card-text">{{$boutique->adresse_boutique}}</span>
                                </li>
                                <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                    <span class="item-icon mr-3"><i class="fas fa-phone"></i></span>
                                    <span class="card-text">
                                        <a href="tel:{{$boutique->telephone_boutique}}">{{$boutique->telephone_boutique}}</a>
                                    </span>
                                </li>
                                <li class="list-group-item bg-transparent d-flex text-dark px-0">
                                    <span class="item-icon mr-3"><i class="fal fa-globe"></i></span>
                                    <span class="card-text"><a href="{{$boutique->site_web_boutique}}" target="_blank">{{$boutique->site_web_boutique}}</a></span>
                                </li>
                                <li class="list-group-item bg-transparent d-flex text-dark px-0 pt-4">
                                    <div class="social-icon origin-color si-square text-center">
                                        <ul class="list-inline">
                                            <li class="list-inline-item si-facebook">
                                                <a target="_blank" title="Facebook" href="#">
                                                    <i class="fab fa-facebook-f">
                                                    </i>
                                                    <span>Facebook</span>
                                                </a>
                                            </li>
                                            <li class="list-inline-item si-twitter">
                                                <a target="_blank" title="Twitter" href="#">
                                                    <i class="fab fa-twitter">
                                                    </i>
                                                    <span>Twitter</span>
                                                </a>
                                            </li>
                                            <li class="list-inline-item si-rss">
                                                <a target="_blank" title="Instagram" href="#">
                                                    <i class="fab fa-instagram"></i>
                                                    <span>RSS</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card p-4 widget border-0 contact bg-white mb-6 rounded-0">
                    <div class="card-title mb-0 border-bottom pb-2 pt-1">
                        <div class="media">
                            <div class="image mr-3">
                                @if ($user->profile_photo_path == null)
                                <img src="{{asset('images/bs.jpeg')}}"
                                    alt="Contactez-moi-bs" class="rounded-circle">
                                @else
                                <img src="{{$user->profile_photo_path}}"
                                    alt="Contactez-moi" class="rounded-circle">
                                @endif
                            </div>
                            <div class="media-body lh-14">
                                <span
                                    class="font-weight-semibold text-dark text-capitalize d-block font-size-md name">
                                    {{$user->prenom_user}} {{$user->nom_user}}
                                </span>
                                <span class="text-gray d-block mb-2">{{$user->adresse_user}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-0 pb-3">
                        <div class="card-title text-uppercase text-dark font-weight-semibold font-size-md">
                            <span class="text-secondary d-inline-block mr-2"><i
                                    class="fas fa-envelope"></i></span>
                            <span>Contactez-moi</span>
                        </div>
                        <div class="contact-form">
                            <form>
                                <div class="form-group mb-2">
                                    <label class="sr-only" for="nom_prenom">Prenom Nom</label>
                                    <input type="text" id="nom_prenom" class="form-control form-control-sm bg-white"
                                        placeholder="Prenom Nom" name="nom_prenom">
                                </div>
                                <div class="form-group mb-2">
                                    <label class="sr-only" for="email">Email</label>
                                    <input type="text" id="email" name="email"
                                        class="form-control form-control-sm bg-white"
                                        placeholder="Adresse email">
                                </div>
                                <div class="form-group mb-2">
                                    <label class="sr-only" for="message">Message</label>
                                    <textarea class="form-control" id="message"
                                        placeholder="Votre message..." name="message"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block btn-sm">Envoyer Message
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('style')
    <style>
        .navbar {
            overflow: hidden;
            background-color: #333;
            position: fixed !important; /* Set the navbar to fixed position */
            top: 0; /* Position the navbar at the top of the page */
            width: 100%; /* Full width */
        }
    </style>
@endsection

@section('script')
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiMIj9qJw-InawUWnu7kUK4GjDQ7dktMQ&amp;callback=initMap"></script>
	<script>
		var map;

		function initMap() {
			var latlng = new google.maps.LatLng(40.762529, -73.957334);

			var mapProp = {
				center: latlng,
				zoom: 12,
				mapTypeId: 'roadmap',
				disableDefaultUI: true,
				styles: [
					{
						"featureType": "administrative.country",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "administrative.province",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "administrative.province",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#c3b6a2"
							}
						]
					},
					{
						"featureType": "landscape.man_made",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "landscape.man_made",
						"elementType": "labels.text",
						"stylers": [
							{
								"color": "#c3b6a2"
							}
						]
					},
					{
						"featureType": "landscape.man_made",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#c3b6a2"
							}
						]
					},
					{
						"featureType": "landscape.natural",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "landscape.natural.landcover",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "landscape.natural.terrain",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "poi.business",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "poi.park",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "poi.park",
						"elementType": "labels.icon",
						"stylers": [
							{
								"color": "#808080"
							}
						]
					},
					{
						"featureType": "poi.park",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#808080"
							}
						]
					},
					{
						"featureType": "poi.school",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ffffff"
							}
						]
					},
					{
						"featureType": "road.arterial",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#f0f0f0"
							}
						]
					},
					{
						"featureType": "road.arterial",
						"elementType": "geometry.stroke",
						"stylers": [
							{
								"color": "#c0c0c0"
							},
							{
								"saturation": -75
							},
							{
								"lightness": -80
							}
						]
					},
					{
						"featureType": "road.arterial",
						"elementType": "labels",
						"stylers": [
							{
								"visibility": "off"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ededed"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.stroke",
						"stylers": [
							{
								"color": "#ededed"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "labels",
						"stylers": [
							{
								"visibility": "off"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "labels.text",
						"stylers": [
							{
								"color": "#ededed"
							}
						]
					},
					{
						"featureType": "road.highway",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#91bbd5"
							}
						]
					},
					{
						"featureType": "road.highway.controlled_access",
						"stylers": [
							{
								"color": "#ededed"
							}
						]
					},
					{
						"featureType": "road.local",
						"stylers": [
							{
								"visibility": "off"
							}
						]
					},
					{
						"featureType": "road.local",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#ededed"
							}
						]
					},
					{
						"featureType": "transit.line",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#f0f0f0"
							}
						]
					},
					{
						"featureType": "transit.station.airport",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#c3b6a2"
							}
						]
					},
					{
						"featureType": "water",
						"elementType": "geometry.fill",
						"stylers": [
							{
								"color": "#c7d7d4"
							}
						]
					},
					{
						"featureType": "water",
						"elementType": "labels.text.fill",
						"stylers": [
							{
								"color": "#91bbd5"
							}
						]
					}
				]
			};
			map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

		}
	</script>
@endsection
@endsection
