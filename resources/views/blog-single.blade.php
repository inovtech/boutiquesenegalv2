@extends('layouts.guest')
@section('content')
    <header id="header"
        class="main-header header-float header-sticky header-sticky-smart header-light header-style-03 font-normal">
        <div class="header-wrapper sticky-area">
            <div class="container">
                <nav class="navbar navbar-expand-xl">
                    <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                        <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                            <span></span>
                        </div>
                        <a class="navbar-brand navbar-brand-mobile" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true"
                            data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'>
                            <i class="far fa-search"></i>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse d-xl-flex" id="navbar-main-menu">
                        <a class="navbar-brand d-none d-xl-block" href="/">
                            <img src="{{ asset('images/white-logo.png') }}" alt="Boutique Senegal" />
                        </a>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="/categorie">Categorie <span class="caret"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/blog">Blog<span class="caret"></i></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/pricing">Offres<span class="caret"></i></span></a>
                            </li>
                            @auth
                                @if (Auth::user()->role_id == 1)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('admin') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>
                                    
                                @elseif(Auth::user()->role_id == 2)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('home') }}"><i class="far fa-tachometer-slowest mr-1"></i>dashboard<span class="caret"></i></span></a>
                                    </li>
                                    
                                @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('login') }}"><i class="fas fa-user-circle mr-1"></i> Se
                                    connecter<span class="caret"></i></span></a>
                            </li>
                            @endauth
                            <li class="nav-item ">
                                <a class="btn btn-primary text-capitalize teest" href="{{ route('client.create-shop') }}"> +
                                    Ajoutez votre boutique<span class="caret"></i></span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <div id="page-title" class="page-title page-title-style-background">
        <div class="container">
            <div class="single-blog-top p-0 h-100 d-flex flex-column justify-content-center">
                <h2 class="text-center mb-3 text-white letter-spacing-50">
                    {{ $article->titre_article }}
                </h2>
                <ul class="list-inline d-flex align-items-center justify-content-center flex-wrap">
                    <li class="list-inline-item mr-1">
                        <span
                            class="text-white">{{ \Carbon\Carbon::parse($article->created_at)->translatedFormat('d F Y') }}
                            par</span>
                    </li>
                    <li class="list-inline-item">
                        <a href="#" class="link-hover-white-primary">{{ $article->prenom_user }}
                            {{ $article->nom_user }}</a>
                    </li>
                    <li class="list-inline-item">
                        <span class="text-white">/</span>
                    </li>
                    <li class="list-inline-item">
                        <a href="" class="link-hover-white-primary">{{ $article->nom_categorie_blog }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="wrapper-content" class="wrapper-content pb-13">
        <div class="container">
            <div class="page-container">
                <div class="page-content">
                    <div class="mb-10">
                        <div class="mb-12">
                            <img src="{{ $article->photo_article }}" alt="{{ $article->titre_article }}" />
                        </div>
                        <div class="single-blog-content">
                            {!! html_entity_decode($article->description_article) !!}
                            <div class="pb-11 text-center">
                                <div class="mb-5">
                                    <span class="text-dark font-weight-semibold">Tags:</span>
                                    <a href="#" class="link-hover-gray-blue">Tips,</a>
                                    <a href="#" class="link-hover-gray-blue">Travel,</a>
                                    <a href="#" class="link-hover-gray-blue">Homestay,</a>
                                    <a href="#" class="link-hover-gray-blue">Experience</a>
                                </div>
                                <div class="d-flex align-items-center pb-2 justify-content-center">
                                    <span class="text-dark font-weight-semibold d-inline-block mr-3">Share:
                                    </span>
                                    <div class="social-icon">
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a target="_blank" title="Facebook" href="#">
                                                    <i class="fab fa-facebook-f"> </i>
                                                    <span>Facebook</span>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a target="_blank" title="Twitter" href="#">
                                                    <i class="fab fa-twitter"> </i>
                                                    <span>Twitter</span>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a target="_blank" title="Twitter" href="#">
                                                    <i class="fab fa-pinterest-p"></i>
                                                    <span>Pinterest</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-9">
                        <div class="mb-8 text-center">
                            <h4 class="mb-0">Articles recents</h4>
                        </div>
                        <div class="row post-style-3">
                            @foreach ($posts as $post)
                                <div class="col-md-4 mb-4 mb-md-0">
                                    <div class="card border-0">
                                        <a href="{{ route('guest.blog-index', [$post->slug_categorie_blog]) }}"
                                            class="hover-scale">
                                            <img src="{{ $post->photo_article }}" alt="product 1"
                                                class="card-img-top image" />
                                        </a>
                                        <div class="card-body px-0">
                                            <div class="mb-2">
                                                <a href="{{ route('guest.blog-index', [$post->slug_categorie_blog]) }}"
                                                    class="link-hover-dark-primary">{{ $post->nom_categorie_blog }}</a>
                                            </div>
                                            <h5 class="card-title">
                                                <a href="{{ route('guest.blog-index-item', [$post->slug_categorie_blog, $post->slug_article]) }}"
                                                    class="link-hover-dark-primary text-capitalize">
                                                    {{ $post->titre_article }}
                                                </a>
                                            </h5>
                                            <ul class="list-inline">
                                                <li class="list-inline-item mr-0">
                                                    <span
                                                        class="text-gray">{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('d F Y') }}
                                                        par</span>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a href="#" class="link-hover-dark-primary">{{ $post->prenom_user }}
                                                        {{ $post->nom_user }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
