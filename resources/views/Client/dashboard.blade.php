@extends('layouts.client')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/Logos.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/Logos.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>

<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Client.Layouts.sidebar')
        <div class="page-container">
            <div class="container-fluid">
                <div class="page-content-wrapper d-flex flex-column justify-content-center">

                    <div class="facts-box mb-6 row">
                        <div class="col-md-4 mb-6 mb-xl-0">
                            <div class="card view rounded-0 border-0 text-white">
                                <div class="card-body d-flex align-items-center p-0">
                                    <div class="content mr-auto">
                                        <span class="font-size-h1 font-weight-semibold lh-1 d-block">4</span>
                                        <span class="font-size-lg">Vues</span>
                                    </div>
                                    <div class="fact-icon">
                                        <svg class="icon icon-eye">
                                            <use xlink:href="#icon-eye"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-6 mb-xl-0">
                            <div class="card review rounded-0 border-0 text-white">
                                <div class="card-body d-flex align-items-center p-0">
                                    <div class="content mr-auto">
                                        <span class="font-size-h1 font-weight-semibold lh-1 d-block">{{$count_shop->count()}}</span>
                                        @if ($count_shop->count() <= 1)
                                            <span class="font-size-lg">Boutique</span>
                                        @else
                                            <span class="font-size-lg">Boutiques</span>
                                        @endif
                                    </div>
                                    <div class="fact-icon">
                                    <i class="fal fa-store"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card time-bookmark rounded-0 border-0 text-white">
                                <div class="card-body d-flex align-items-center p-0">
                                    <div class="content mr-auto">
                                        <span class="font-size-h1 font-weight-semibold lh-1 d-block">{{$count_product_shop->count()}}</span>
                                        @if ($count_product_shop->count() <= 1)
                                            <span class="font-size-lg">Produit</span>
                                        @else
                                            <span class="font-size-lg">Produits</span>
                                        @endif
                                    </div>
                                    <div class="fact-icon">
                                    <i class="fal fa-shopping-bag"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="features row">
                        <div class="card rounded-0 col-md-3 col-lg-3 border-0 bg-transparent mb-6">
                            <div class="card-body d-flex align-items-center py-6 px-8 bg-white">
                                <span class="font-size-h1 font-weight-semibold d-inline-block mr-2 text-primary lh-1">{{$count_shop->count()}}</span>
                                @if ($count_shop->count() <= 1)
                                    <span class="font-size-md font-weight-semibold text-uppercase text-dark lh-13">Boutique <br> Totale</span>
                                @else
                                    <span class="font-size-md font-weight-semibold text-uppercase text-dark lh-13">Boutiques <br> Totales</span>
                                @endif
                            </div>
                        </div>
                        <div class="card rounded-0 col-md-3 col-lg-3 border-0 bg-transparent mb-6">
                            <div class="card-body d-flex align-items-center py-6 px-8 bg-white">
                                <span class="font-size-h1 font-weight-semibold d-inline-block mr-2 text-darker-light lh-1">{{$count_wait_shop->count()}}</span>
                                @if ($count_wait_shop->count() <= 1)
                                    <span class="font-size-md font-weight-semibold text-uppercase text-dark lh-13">Boutique <br> en Attente</span>
                                @else
                                    <span class="font-size-md font-weight-semibold text-uppercase text-dark lh-13">Boutiques <br> en Attente</span>
                                @endif
                            </div>
                        </div>
                        <div class="card rounded-0 col-md-3 col-lg-3 border-0 bg-transparent mb-6">
                            <div class="card-body d-flex align-items-center py-6 px-8 bg-white">
                                <span class="font-size-h1 font-weight-semibold d-inline-block mr-2 lh-1 published">{{$count_premium_shop->count()}}</span>
                                @if ($count_premium_shop->count() <= 1)
                                    <span class="font-size-md font-weight-semibold text-uppercase text-dark lh-13">Boutique <br> premium</span>
                                @else
                                    <span class="font-size-md font-weight-semibold text-uppercase text-dark lh-13">Boutiques <br> premium</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="d-flex two-column mb-13 mx-n3">
                        <div class="page-left mb-6 px-3">

                            <div class="row tables">
                                <div class="col-xl-6 mb-6 mb-xl-0">
                                    <div class="card rounded-0 border-0 reccent-activities">
                                        <div class="card-body">
                                            <h5 class="card-title text-capitalize border-bottom pb-2 mb-2">
                                            Activités
                                            Récentes</h5>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="icon-box no-shape icon-box-style-03 d-flex">
                                                                    <span class="icon-box-icon">
                                                                        <svg class="icon icon-layers"><use xlink:href="#icon-layers"></use></svg>
                                                                    </span>
                                                                    <div class="content-box">Votre boutique
                                                                        <a href="listing-details-full-image.html" class="font-weight-semibold link-hover-dark-primary">Djolof Sport
                                                                        </a>
                                                                        a été approuvée!
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>Il y a 01 h</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="icon-box no-shape icon-box-style-03 d-flex">
                                                                    <span class="icon-box-icon">
                                                                        <i class="fal fa-bookmark"></i>
                                                                    </span>
                                                                    <div class="content-box">
                                                                        <a href="#" class="font-weight-semibold link-hover-dark-primary">Abdou Fall</a>
                                                                        a sauvé votre boutique
                                                                        <a href="listing-details-full-image.html" class="font-weight-semibold link-hover-dark-primary">Djolof Sport</a> !
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>Il y a 15 mn</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="icon-box no-shape icon-box-style-03 d-flex">
                                                                    <span class="icon-box-icon">
                                                                        <i class="fal fa-star"></i>
                                                                    </span>
                                                                    <div class="content-box">
                                                                        <a href="#" class="font-weight-semibold link-hover-dark-primary">Anna Ndiaye</a>
                                                                        laissé une note de
                                                                        <a href="panel-review-submitted.html" class="font-weight-semibold link-hover-dark-primary">5
                                                                        Étoiles</a>
                                                                        à votre boutique
                                                                        <a href="#" class="font-weight-semibold link-hover-dark-primary">
                                                                        Djolof Sport</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>Il y a 2 mn</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="card rounded-0 border-0 invoices">
                                        <div class="card-body">
                                            <h5 class="card-title text-capitalize border-bottom pb-2 mb-2">
                                            Boutique</h5>
                                            <div class="table-responsive-sm">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th>NOM</th>
                                                            <th>CATEGORIE</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($boutiques as $boutique)
                                                            <tr>
                                                                <td>
                                                                    <a href="#" class="link-hover-dark-blue">#{{$boutique->id}}</a>
                                                                </td>
                                                                <td>{{$boutique->nom_boutique}}</td>
                                                                <td>{{$boutique->nom_categorie_boutique}}</td>
                                                                @if ($boutique->is_active_boutique == true)
                                                                    <td><span class="status paid">Active</span></td>
                                                                @else
                                                                    <td><span class="status text-primary">en Attente</span></td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="copy-right mt-auto text-center">&copy;
                        <script>document.write( new Date().getFullYear() );</script>
                        <a href="/" class="link-hover-dark-primary font-weight-semibold">Boutique Senegal</a>
                        by
                        <a href="https://inovtechsenegal.com" class="link-hover-dark-primary font-weight-semibold">Inovtech</a>
                        Tous droits réservés.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
