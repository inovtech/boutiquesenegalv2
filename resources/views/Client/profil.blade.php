@extends('layouts.app')
@section('content')
<header id="header" class="main-header header-sticky header-sticky-smart header-style-10 text-uppercase bg-white">
    <div class="header-wrapper sticky-area border-bottom">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-xl">
                <div class="header-mobile d-flex d-xl-none flex-fill justify-content-between align-items-center">
                    <div class="navbar-toggler toggle-icon" data-toggle="collapse" data-target="#navbar-main-menu">
                        <span></span>
                    </div>
                    <a class="navbar-brand navbar-brand-mobile" href="{{url('/')}}">
                        <img src="{{asset('images/Logos.png')}}" alt="TheDir">
                    </a>
                    <a class="mobile-button-search" href="#search-popup" data-gtf-mfp="true" data-mfp-options='{"type":"inline","mainClass":"mfp-move-from-top mfp-align-top search-popup-bg","closeOnBgClick":false,"showCloseBtn":false}'><i class="far fa-search"></i></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-main-menu">
                    <a class="navbar-brand d-none d-xl-block" href="{{url('/')}}">
                        <img src="{{asset('images/Logos.png')}}" alt="TheDir">
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>


<div id="wrapper-content" class="wrapper-content pt-0 pb-0">
    <div class="page-wrapper d-flex flex-wrap flex-xl-nowrap">
        @include('Client.Layouts.sidebar')
        <div class="page-container">
            <div class="container-fluid">
                <div class="page-content-wrapper d-flex flex-column">
                    <h1 class="font-size-h4 mb-4 font-weight-normal">Mon Profil</h1>
                    <div class="row  two-column mb-13 mx-n3">
                        <div class="col-lg-6 mb-4 mb-lg-0">
                            <div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
                                <div class="card-header p-0 bg-transparent"><h5 class="card-title text-capitalize">
                                Détails profil</h5>
                                </div>
                                <div class="card-body px-0 pt-4 pb-0">
                                    <div class="profile media d-flex align-items-center flex-wrap">
                                        <div class="image mb-4">
                                            @if ($profil->profile_photo_path)
                                                <img src="{{ asset($profil->profile_photo_path) }}" alt="Photo {{$profil->prenom_user}} {{$profil->nom_user}}" class="rounded-circle">
                                            @else
                                                <img src="images/other/none-img.png" alt="User image" width="150"  class="img-fluid rounded-circle">
                                            @endif
                                        </div>
                                        <div class="media-body d-flex flex-wrap">
                                            <div class="upload-btn-wrapper mr-4 ml-4 mb-4">
                                                <button class="btn btn-primary px-4 font-size-md">Changer
                                                de photo
                                                </button>
                                                <input type="file" name="profile_photo_path">
                                            </div>
                                        <a href="#" class="btn btn-darker-light mb-4 font-size-md">Supprimer</a>
                                        </div>
                                    </div>
                                    <!-- <div class="profile media d-flex align-items-center flex-wrap">
                                        <div class="image mb-4">
                                            <img src="{{ asset($profil->profile_photo_path) }}" alt="User image" class="rounded-circle">
                                        </div>
                                        <div class="media-body d-flex flex-wrap">
                                            <div class="upload-btn-wrapper mr-4 mb-4">
                                                <button class="btn btn-primary px-4 font-size-md">Changer
                                                 de photo
                                                </button>
                                                <input type="file" name="profile_photo_path" />
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="">
                                        <form action="{{ route('client.update-auth') }}" method="POST" enctype="multipart/form-data">
                                            @method('GET')
                                            @csrf
                                            <div class="form-row mb-2">
                                                <div class="col-sm-6 mb-2 mb-sm-0">
                                                    <label for="prenom_user" class="font-size-md text-dark font-weight-semibold mb-1">Prénom(s)
                                                    <span class="text-danger">*</span></label>
                                                    <input class="form-control @error('prenom_user') border border-danger @enderror" id="prenom_user" name="prenom_user" type="text" value="{{$profil->prenom_user}}" required>
                                                    @error('prenom_user')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="nom_user" class="font-size-md text-dark font-weight-semibold mb-1">Nom
                                                    <span class="text-danger">*</span></label>
                                                    <input class="form-control @error('nom_user') border border-danger @enderror" id="nom_user" name="nom_user" type="text" value="{{$profil->nom_user}}" required>
                                                    @error('nom_user')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-row mb-2">
                                                <div class="col-sm-6 mb-2 mb-sm-0">
                                                    <label for="email" class="font-size-md text-dark font-weight-semibold mb-1">Email
                                                    <span class="text-danger">*</span></label>
                                                    <input class="form-control @error('email') border border-danger @enderror" id="email" type="text" name="email" value="{{$profil->email}}">
                                                    @error('email')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="telephone_user" class="font-size-md text-dark font-weight-semibold mb-1">Téléphone
                                                    <span class="text-secondary font-weight-normal">(Whatsapp)</span></label>
                                                    <input class="form-control @error('telephone_user') border border-danger @enderror" id="telephone_user" type="text" name="telephone_user" value="{{$profil->telephone_user}}">
                                                    @error('telephone_user')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group mb-2">
                                                <label for="adresse_user" class="font-size-md text-dark font-weight-semibold mb-1">Adresse domicile</label>
                                                <input class="form-control @error('adresse_user') border border-danger @enderror" id="adresse_user" name="adresse_user" value="{{$profil->adresse_user}}">
                                                @error('adresse_user')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block font-size-lg">Sauvegarder
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card rounded-0 border-0 bg-white px-4 pt-3 pb-6">
                                <div class="card-header p-0 bg-transparent">
                                    <h5 class="card-title text-capitalize">Changer de Mot de Passe</h5>
                                </div>
                                <div class="card-body px-0 pt-4 pb-0">
                                    <div class="form-update-profile">
                                        <form method="PUT" action="{{ route('client.user-change-pwd') }}">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group mb-2">
                                                <label for="ancien_mot_de_passe" class="font-size-md text-dark font-weight-semibold mb-1">Ancien mot de passe</label>
                                                <input type="password" class="form-control @error('ancien_mot_de_passe') border border-danger @enderror" id="ancien_mot_de_passe" name="ancien_mot_de_passe">
                                                @error('ancien_mot_de_passe')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-2">
                                                <label for="password" class="font-size-md text-dark font-weight-semibold mb-1">Mot de passe</label>
                                                <input type="password" class="form-control @error('password') border border-danger @enderror" id="password" name="password">
                                                @error('password')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-6">
                                                <label for="password_confirmation" class="font-size-md text-dark font-weight-semibold mb-1">Confirmer mot de passe</label>
                                                <input type="password" class="form-control @error('password_confirmation') border border-danger @enderror" id="password_confirmation" name="password_confirmation">
                                                @error('password_confirmation')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-block font-size-lg">Modifier
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="copy-right mt-auto text-center">&copy;
                        <script>document.write( new Date().getFullYear() );</script>
                        <a href="/" class="link-hover-dark-primary font-weight-semibold">Boutique Senegal</a>
                        by
                        <a href="https://inovtechsenegal.com" class="link-hover-dark-primary font-weight-semibold">Inovtech</a>
                        Tous droits réservés.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
