<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentaireBoutiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commentaire_boutiques', function (Blueprint $table) {
            $table->id();
            $table->string('nom_prenom');
            $table->string('email_commentaire_boutique');
            $table->string('objet_commentaire_boutique');
            $table->integer('boutique_id')->unsigned();
            $table->text('commentaire_boutique');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commentaire_boutiques');
    }
}
