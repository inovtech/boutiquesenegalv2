<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoutiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boutiques', function (Blueprint $table) {
            $table->id();
            $table->integer('user_boutique_id')->unsigned();
            $table->integer('categorie_boutique_id')->unsigned();
            $table->string('nom_boutique');
            $table->string('slug_nom_boutique');
            $table->string('adresse_boutique');
            $table->string('ville_boutique');
            $table->string('telephone_boutique');
            $table->string('email_boutique');
            $table->string('site_web_boutique')->nullable();
            $table->string('link_facebook_boutique')->nullable();
            $table->string('link_instagram_boutique')->nullable();
            $table->string('link_twitter_boutique')->nullable();
            $table->string('jour_ouvrable_boutique')->nullable();
            $table->time('open_time_boutique')->nullable();
            $table->time('close_time_boutique')->nullable();
            $table->text('description_boutique');
            $table->string('photo_boutique')->nullable();
            $table->string('map_url_boutique')->nullable();
            $table->integer('pack_boutique_id')->unsigned();
            $table->boolean('is_active_boutique')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boutiques');
    }
}
