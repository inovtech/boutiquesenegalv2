<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id();
            $table->string('nom_produit');
            $table->string('slug_nom_produit');
            $table->integer('boutique_id')->unsigned();
            $table->integer('categorie_produit_id')->unsigned();
            $table->text('description_produit')->nullable();
            $table->integer('prix_produit');
            $table->string('photo_produit')->nullable();
            $table->string('commander_produit')->nullable();
            $table->boolean('is_active_produit')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
}
