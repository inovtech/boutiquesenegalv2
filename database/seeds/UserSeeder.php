<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = [

            [
                'id' => 1,
                'nom_user' => 'BADJI',
                'prenom_user' => 'Thiaka',
                'email' => 'badji@boutiquesenegal.com',
                'password' => bcrypt('boutiquev2'),
                'password_change_at' => null,
                'adresse_user' => 'Mbour, Senegal',
                'telephone_user' => '777779260',
                'role_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'id' => 2,
                'nom_user' => 'NDIAYE',
                'prenom_user' => 'Ibrahima',
                'email' => 'ndiaye@boutiquesenegal.com',
                'password' => bcrypt('boutiquev2'),
                'password_change_at' => null,
                'adresse_user' => 'Dakar, Senegal',
                'telephone_user' => '777484804',
                'role_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'id' => 3,
                'nom_user' => 'SARR',
                'prenom_user' => 'Tony',
                'email' => 'tony@boutiquesenegal.com',
                'password' => bcrypt('boutiquev2'),
                'password_change_at' => null,
                'adresse_user' => 'Dakar, Senegal',
                'telephone_user' => '777796678',
                'role_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            
            [
                'id' => 4,
                'nom_user' => 'NDIAYE',
                'prenom_user' => 'IBOU',
                'email' => 'djolofsport@boutiquesenegal.com',
                'password' => bcrypt('boutiquev2'),
                'password_change_at' => null,
                'adresse_user' => 'Dakar, Senegal',
                'telephone_user' => '777796678',
                'role_id' => 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        DB::table('users')->insert($user);
    }
}
