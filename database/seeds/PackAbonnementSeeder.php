<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PackAbonnementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pack = [
            [
                'nom_pack_abonnement' => 'Gratuit',
                'prix_pack_abonnement' => 0,
                'description_pack_abonnement' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_pack_abonnement' => 'Decouvert',
                'prix_pack_abonnement' => 10000,
                'description_pack_abonnement' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_pack_abonnement' => 'Premium',
                'prix_pack_abonnement' => 50000,
                'description_pack_abonnement' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];
        DB::table('pack_abonnements')->insert($pack);
    }
}
