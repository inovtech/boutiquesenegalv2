<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TypeUserSeeder::class,
            UserSeeder::class,
            PackAbonnementSeeder::class,
            CategorieBoutiqueSeeder::class,
            SousCategorieBoutiqueSeeder::class,
            CategorieBlogSeeder::class,
            CategorieProduitSeeder::class,
        ]);
    }
}
