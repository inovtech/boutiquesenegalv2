<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CategorieBlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $catblog = [
            [
                'nom_categorie_blog' => 'Informatique',
                'slug_categorie_blog' => 'informatique',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_blog' => 'Entrepreneuriat',
                'slug_categorie_blog' => 'entrepreneuriat',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_blog' => 'Data Scientist',
                'slug_categorie_blog' => 'data-scientist',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_blog' => 'Ecommerce',
                'slug_categorie_blog' => 'ecommerce',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],

            [
                'nom_categorie_blog' => 'Intelligence Artificielle',
                'slug_categorie_blog' => 'intelligence-artificielle',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];
        DB::table('categorie_blogs')->insert($catblog);
    }
}
