<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GuestController@index')->name('index');

// Route::get('/blog-single', function () {
//     return view('blog-single');
// });

Route::get('/pricing', function () {
    return view('price');
});
Route::get('/recherche', 'GuestController@search')->name('guest.search');
Route::get('/boutique-du-jour', 'GuestController@shop_of_z_day')->name('guest.shop_of_z_day');

Route::get('/blog', 'GuestController@index_blog');
Route::get('/blog/{slug_categorie_blog}', 'GuestController@blog_categorie_index')->name('guest.blog-index');
Route::get('/blog/{slug_categorie_blog}/{slug_article}', 'GuestController@blog_index')->name('guest.blog-index-item');
Route::get('/categorie', 'GuestController@categorie');
Route::get('/boutique/{slug_nom_boutique}', 'GuestController@boutique_index')->name('guest.boutique-index');
Route::get('/boutique/{slug_nom_boutique}/{slug_nom_produit}', 'GuestController@produit_index')->name('guest.produit-index');
Route::get('/categorie/{nom_categorie_boutique}', 'GuestController@categorie_index')->name('guest.categorie-index');


Auth::routes();

// Route Profil CLIENT
Route::middleware(['auth', 'client'])->group(function() {
    // Dashboard
    Route::get('/dashboard', 'HomeController@index')->name('home');
    Route::get('/profil', 'ClientController@profil')->name('client.profil');
    Route::get('/auth_user_update', 'ClientController@update_auth_profile')->name('client.update-auth');
    Route::get('/user-change-password', 'ClientController@update_pwd')->name('client.user-change-pwd');
    Route::get('/boutiques', 'ClientController@index_boutique')->name('client.index-shop');

    Route::get('/produit', 'ClientController@index_produit')->name('client.index-produit');
    Route::get('/boutiques/create', 'ClientController@create_boutique')->name('client.create-shop');
    Route::get('/produit/create', 'ClientController@create_produit')->name('client.create-produit');
    Route::post('/boutiques/store', 'ClientController@store_boutique')->name('client.store-shop');
    Route::post('/produit/store', 'ClientController@store_produit')->name('client.store-produit');
    Route::get('/boutiques/{id}/edit', 'ClientController@edit_boutique')->name('client.edit-shop');
    Route::get('/boutiques/update', 'ClientController@update_boutique')->name('client.update-shop');
    Route::get('/produit/update', 'ClientController@update_produit')->name('client.update-produit');
    Route::get('/produit/{id}/edit', 'ClientController@edit_produit')->name('client.edit-produit');
    Route::get('/removeImageFromGallerie', 'ClientController@removeImageFromGallerie');

    Route::delete('/produit/{id}/delete', 'ClientController@destroy_produit')->name('client.delete-produit');

    //message
    Route::resource('/message', 'MessageController');
});

// Route Profil ADMINISTRATEUR
Route::middleware(['auth', 'admin'])->prefix('admin')->group(function() {
    // Dashboard
    Route::get('/dashboard', 'HomeController@admin')->name('admin');

    // Pack Abonnement resource
    Route::resource('/pack-abonnement', 'PackAbonnementController');

    // Categorie Boutique resource
    Route::resource('/categorie-boutique', 'CategorieBoutiqueController');

    // Sous Categorie Boutique resource
    Route::resource('/sous-categorie-boutique', 'SousCategorieBoutiqueController');

    //  Categorie produit resource
    Route::resource('/categorie-produit', 'CategorieProduitController');

    //  Categorie Blog resource
    Route::resource('/categorie-blog', 'CategorieBlogController');

    //  Categorie Blog resource
    Route::resource('/tag', 'TagController');

    //  Article resource
    Route::resource('/article', 'ArticleController');

    //  Article resource
    Route::resource('/boutique', 'BoutiqueController');

    Route::get('/boutique_statut', 'BoutiqueController@statut')->name('boutique.statut');

    //  produit resource
    Route::resource('/produits', 'ProduitController');

    //  User resource
    Route::resource('/utilisateur', 'UserController');
    Route::get('/profil', 'UserController@profil')->name('utilisateur.profil');
    Route::get('/auth_user_update', 'UserController@update_auth_profile')->name('utilisateur.update-auth');
    Route::get('/user-change-password', 'UserController@update_pwd')->name('utilisateur.user-change-pwd');
    Route::get('/nouveau-admin/create', 'UserController@create_admin');
    Route::get('/edit-admin/{id}/edit', 'UserController@edit_admin')->name('utilisateur.edit-a');
    Route::post('/nouveau-admin', 'UserController@store_admin')->name('utilisateur.store-admin');
    Route::put('/edit-admin/{id}/edit', 'UserController@update_admin')->name('utilisateur.edit-admin');

    Route::get('/nouveau-client/create', 'UserController@create_client');
    Route::get('/edit-client/{id}/edit', 'UserController@edit_client')->name('utilisateur.edit-c');
    Route::post('/nouveau-client', 'UserController@store_client')->name('utilisateur.store-client');
    Route::put('/edit-client/{id}', 'UserController@update_client')->name('utilisateur.edit-client');
    Route::get('/removeImageFromGallerie', 'BoutiqueController@removeImageFromGallerie');

});
